package plugins.adufour.roi;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.poi.ss.usermodel.Workbook;
import org.math.plot.Plot2DPanel;
import org.math.plot.plots.Plot;

import icy.file.FileUtil;
import icy.gui.dialog.SaveDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.util.GuiUtil;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import plugins.adufour.activecontours.ActiveContour;
import plugins.adufour.activecontours.ActiveContours.ROIType;
import plugins.adufour.blocks.tools.io.WorkbookToFile;
import plugins.adufour.blocks.tools.io.WorkbookToFile.MergePolicy;
import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarChannel;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarListener;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;
import plugins.kernel.roi.roi3d.ROI3DArea;
import plugins.nchenouard.particletracking.DetectionSpotTrack;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Point3D;

public class ROIStatisticsTrackProcessor extends PluginTrackManagerProcessor implements PluginBundled
{
    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
    
    private final JButton      exportButton    = new JButton("Export to XLS...");
    
    private final JComboBox    jComboDescriptors;
    
    private VarSequence        sequence        = new VarSequence("Sequence", null);
    
    private VarChannel         channel         = new VarChannel("Channel", sequence, false);
    
    private VarEditor<Integer> channelSelector = channel.createVarEditor();
    
    private final JPanel       chartPanel;
    
    private final Plot2DPanel  plotPanel;
    
    private static class ROIDescriptorWrapper implements Comparable<ROIDescriptorWrapper>
    {
        final ROIDescriptor descriptor;
        
        public ROIDescriptorWrapper(ROIDescriptor descriptor)
        {
            this.descriptor = descriptor;
        }
        
        @Override
        public String toString()
        {
            return descriptor.getName();
        }
        
        @Override
        public int compareTo(ROIDescriptorWrapper descriptor)
        {
            return toString().compareTo(descriptor.toString());
        }
    }
    
    public ROIStatisticsTrackProcessor()
    {
        super.getDescriptor().setDescription("Monitor shape and intensity over time");
        super.setName("ROI Statistics");
        
        // get all descriptors available
        Map<ROIDescriptor, PluginROIDescriptor> map = icy.roi.ROIUtil.getROIDescriptors();
        
        // keep descriptors producing numbers
        Vector<ROIDescriptorWrapper> descriptors = new Vector<ROIDescriptorWrapper>(map.size());
        for (ROIDescriptor desc : map.keySet())
            if (Number.class.isAssignableFrom(desc.getType())) descriptors.addElement(new ROIDescriptorWrapper(desc));
        
        // sort them by name
        Collections.sort(descriptors);
        
        jComboDescriptors = new JComboBox(descriptors);
        
        super.panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        
        super.panel.add(Box.createVerticalStrut(5));
        
        exportButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                export();
            }
        });
        exportButton.setEnabled(false);
        
        super.panel.add(GuiUtil.createLineBoxPanel(Box.createHorizontalStrut(10), new JLabel("Plot:"), Box.createHorizontalStrut(5), jComboDescriptors,
                Box.createHorizontalStrut(10), new JLabel("Channel:"), Box.createHorizontalStrut(5), (JComponent) channelSelector.getEditorComponent(),
                Box.createHorizontalStrut(10), exportButton, Box.createHorizontalStrut(10)));
        
        jComboDescriptors.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Compute();
            }
        });
        
        channel.addListener(new VarListener<Integer>()
        {
            
            @Override
            public void valueChanged(Var<Integer> source, Integer oldValue, Integer newValue)
            {
                Compute();
            }
            
            @Override
            public void referenceChanged(Var<Integer> source, Var<? extends Integer> oldReference, Var<? extends Integer> newReference)
            {
                
            }
        });
        
        super.panel.add(chartPanel = new JPanel());
        
        plotPanel = new Plot2DPanel();
        plotPanel.setPreferredSize(new Dimension(500, 300));
    }
    
    @Override
    public void Close()
    {
        channelSelector.setEnabled(false);
    }
    
    @Override
    public void Compute()
    {
        chartPanel.removeAll();
        
        if (!super.isEnabled()) return;
        
        Sequence seq = trackPool.getDisplaySequence();
        
        if (seq == null)
        {
            chartPanel.add(new JLabel("Cannot compute descriptors without a display sequence"));
            return;
        }
        
        sequence.setValue(seq);
        
        ROIDescriptorWrapper descriptor = (ROIDescriptorWrapper) jComboDescriptors.getSelectedItem();
        String name = descriptor.descriptor.getName();
        String unit = descriptor.descriptor.getUnit(seq);
        String title = name + " (" + (unit == null ? "a.u." : unit) + ")";
        
        plotPanel.removeAllPlots();
        plotPanel.setAxisLabels("Time (sec.)", title);
        
        channelSelector.setEnabled(descriptor.descriptor.separateChannel());
        
        String[] trackIDs = new String[trackPool.getTrackSegmentList().size()];
        
        try
        {
            int trackID = 0;
            
            track:
            for (TrackSegment ts : trackPool.getTrackSegmentList())
            {
                TrackGroup trackGroup = trackPool.getTrackGroupContainingSegment(ts);
                
                int id = trackGroup.getTrackSegmentList().indexOf(ts);
                trackIDs[trackID] = trackGroup.getDescription() + " #" + id;
                
                List<Detection> detections = ts.getDetectionList();
                
                double[][] xy = new double[detections.size()][];
                
                int detID = 0;
                for (Detection det : detections)
                {
                    ROI roi = null;
                    
                    if (det instanceof DetectionSpotTrack)
                    {
                        roi = new ROI3DArea();
                        for (Point3D pt : ((DetectionSpotTrack) det).spot.point3DList)
                            ((ROI3DArea) roi).addPoint((int) pt.x, (int) pt.y, (int) pt.z);
                    }
                    else if (det instanceof ActiveContour)
                    {
                        roi = ((ActiveContour) det).toROI(ROIType.AREA, seq);
                    }
                    
                    if (roi == null) continue track;
                    
                    if (descriptor.descriptor.separateChannel()) roi = roi.getSubROI(-1, -1, channel.getValue());
                    
                    xy[detID++] = new double[] { det.getT() * seq.getTimeInterval(), (Double) descriptor.descriptor.compute(roi, seq) };
                }
                
                plotPanel.addLinePlot(trackIDs[trackID], ts.getFirstDetection().getColor(), xy);
                
                trackID++;
            }
            
            if (plotPanel.getPlots().size() > 0)
            {
                chartPanel.add(plotPanel);
                exportButton.setEnabled(true);
            }
            else
            {
                chartPanel.add(new JLabel("No statistics available. This may happen when tracking particles (with no shape)"));
                exportButton.setEnabled(false);
            }
        }
        catch (Exception e)
        {
            chartPanel.add(new JLabel("Cannot compute descriptor: " + e.getMessage()));
            exportButton.setEnabled(false);
        }
        
        super.panel.updateUI();
    }
    
    @Override
    public void displaySequenceChanged()
    {
        sequence.setValue(trackPool.getDisplaySequence());
        Compute();
    }
    
    private void export()
    {
        if (plotPanel.getPlots().size() == 0) return;
        
        new Thread()
        {
            public void run()
            {
                ROIDescriptorWrapper descriptor = (ROIDescriptorWrapper) jComboDescriptors.getSelectedItem();
                String name = descriptor.descriptor.getName();
                
                // Restore last used folder
                String xlsFolder = getPreferencesRoot().get("xlsFolder", null);
                
                String path = SaveDialog.chooseFile("Export statistics", xlsFolder, name, ".xls");
                
                if (path == null) return;
                
                // store the folder in the preferences
                getPreferencesRoot().put("xlsFolder", FileUtil.getDirectory(path));
                
                AnnounceFrame message = new AnnounceFrame("Exporting statistics...", 0);
                try
                {
                    Workbook wb = Workbooks.createEmptyWorkbook();
                    IcySpreadSheet sheet = Workbooks.getSheet(wb, name);
                    
                    double tScale = trackPool.getDisplaySequence().getTimeInterval();
                    
                    // write the time column
                    sheet.setValue(0, 0, "Time (sec.)");
                    for (int t = 0; t < trackPool.getDisplaySequence().getSizeT(); t++)
                        sheet.setValue(t + 1, 0, t * tScale);
                    
                    int column = 1;
                    for (Plot plot : plotPanel.getPlots())
                    {
                        double[][] data = plot.getData();
                        
                        sheet.setValue(0, column, plot.getName());
                        for (double[] xy : data)
                        {
                            // retrieve the row from the time step
                            int row = 1 + (int) Math.round(xy[0] / tScale);
                            sheet.setValue(row, column, xy[1]);
                        }
                        column++;
                    }
                    
                    WorkbookToFile.saveAsSpreadSheet(wb, path, MergePolicy.Overwrite);
                }
                finally
                {
                    message.close();
                }
            }
        }.start();
    }
}
