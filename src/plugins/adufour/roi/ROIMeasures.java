package plugins.adufour.roi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JSeparator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

import icy.file.FileUtil;
import icy.gui.main.GlobalSequenceListener;
import icy.main.Icy;
import icy.preferences.XMLPreferences;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROI5D;
import icy.roi.ROIDescriptor;
import icy.roi.ROIEvent;
import icy.roi.ROIEvent.ROIEventType;
import icy.roi.ROIListener;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.sequence.SequenceListener;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.system.thread.ThreadUtil;
import icy.type.point.Point3D;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzDialog;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.roi.ROIConvexHullDescriptor.ROIConvexity;
import plugins.adufour.roi.ROIFeretDiameterDescriptor.ROIFeretDiameter;
import plugins.adufour.roi.ROIRoundnessDescriptor.ROIRoundness;
import plugins.adufour.roi.ROISphericityDescriptor.ROISphericity;
import plugins.adufour.vars.gui.swing.WorkbookEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarTrigger;
import plugins.adufour.vars.lang.VarTrigger.TriggerListener;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.vars.util.VarReferencingPolicy;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi3d.ROI3DArea;

/**
 * Basic measures on regions of interest
 * 
 * @author Alexandre Dufour
 */
public class ROIMeasures extends EzPlug implements ROIBlock, GlobalSequenceListener, SequenceListener, ROIListener
{
    public enum Measures
    {
        FULLPATH("Full path"),
        FOLDER("Parent folder"),
        DATASET("Dataset"),
        NAME("ROI"),
        COLOR("Color"),
        X("X"),
        Y("Y"),
        Z("Z"),
        T("T"),
        C("C"),
        BOX_WIDTH("Box width"),
        BOX_HEIGHT("Box height"),
        BOX_DEPTH("Box depth"),
        CONTOUR("Contour"),
        INTERIOR("Interior"),
        SPHERICITY("Sphericity (%)"),
        ROUNDNESS("Roudness (%)"),
        CONVEXITY("Convexity (%)"),
        MAX_FERET("Max. Feret diam."),
        ELLIPSE_A("Ellipse (a)"),
        ELLIPSE_B("Ellipse (b)"),
        ELLIPSE_C("Ellipse (c)"),
        YAW("Yaw angle"),
        PITCH("Pitch angle"),
        ROLL("Roll angle"),
        ELONGATION("Elongation ratio"),
        FLATNESS3D("Flatness ratio (3D)"),
        INTENSITY_MIN("Min. "),
        INTENSITY_AVG("Avg. "),
        INTENSITY_MAX("Max. "),
        INTENSITY_SUM("Sum "),
        INTENSITY_STD("Std. dev. "),
        INTENSITY_TEXTURE_ASM("Angular second moment "),
        INTENSITY_TEXTURE_CONT("Contrast "),
        INTENSITY_TEXTURE_ENT("Entropy "),
        INTENSITY_TEXTURE_HOMO("Homogeneity ");
        
        Measures(String name)
        {
            this.name = name;
        }
        
        final String name;
        
        boolean      selected = true;
        
        public void toggleSelection()
        {
            selected = !selected;
        }
        
        @Override
        public String toString()
        {
            return name;
        }
        
        public boolean isSelected()
        {
            return selected;
        }
        
        public int getColumnIndex()
        {
            if (!isSelected()) return -1;
            
            Measures[] allMeasures = values();
            int deselectedMeasures = 0;
            for (int i = 0; i < allMeasures.length; i++)
            {
                if (allMeasures[i] == this) return i - deselectedMeasures;
                if (!allMeasures[i].isSelected()) deselectedMeasures++;
            }
            
            // should never happen
            return -1;
        }
        
        public static void saveSelectionToPreferences()
        {
            XMLPreferences prefs = getPreferences("measures");
            for (Measures measure : Measures.values())
                prefs.node(measure.name()).putBoolean("selected", measure.isSelected());
        }
    }
    
    Processor         cpus                    = new Processor(65536, SystemUtil.getNumberOfCPUs());
    final VarTrigger  measureSelector;
    final VarROIArray rois                    = new VarROIArray("Regions of interest");
    final VarWorkbook book                    = new VarWorkbook("Workbook", (Workbook) null);
    final VarSequence sequence                = new VarSequence("Sequence", null);
    
    boolean           measureSelectionChanged = false;
    
    public ROIMeasures()
    {
        XMLPreferences prefs = getPreferences("measures");
        for (Measures measure : Measures.values())
            measure.selected = prefs.node(measure.name()).getBoolean("selected", true);
        
        measureSelector = new VarTrigger("Select features...", new MeasureSelector());
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        measureSelector.setReferencingPolicy(VarReferencingPolicy.NONE);
        inputMap.add("measures", measureSelector);
        inputMap.add("Regions of interest", rois);
        inputMap.add("Sequence", sequence);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Workbook", book);
    }
    
    @Override
    protected void initialize()
    {
        getUI().setActionPanelVisible(false);
        
        addComponent((JComponent) measureSelector.createVarEditor(true).getEditorComponent());
        
        WorkbookEditor viewer = new WorkbookEditor(book);
        viewer.setReadOnly(true);
        viewer.setEnabled(true);
        viewer.setFirstRowAsHeader(true);
        viewer.setOpenButtonVisible(false);
        JComponent jc = viewer.getEditorComponent();
        jc.setPreferredSize(new Dimension(400, 300));
        addComponent(jc);
        
        if (!isHeadLess()) Icy.getMainInterface().addGlobalSequenceListener(this);
        
        getUI().clickRun();
    }
    
    @Override
    public void execute()
    {
        Workbook wb = book.getValue();
        
        // Create the workbook anew if:
        // - it did not exist
        // - we are in block mode (forget previous results)
        if (wb == null || isHeadLess())
        {
            book.setValue(wb = Workbooks.createEmptyWorkbook());
        }
        
        if (!isHeadLess()) for (Sequence sequence : getSequences())
        {
            sequence.addListener(this);
            for (ROI roi : sequence.getROIs())
                roi.addListener(this);
        }
        
        updateStatistics();
    }
    
    private String getDataSetName(Sequence sequence)
    {
        String dataSetName = "ROI Statistics";
        
        if (sequence == null)
        {
            // try retrieving the sequence attached to the first ROI
            List<Sequence> sequences = rois.getValue()[0].getSequences();
            if (sequences.size() > 0) sequence = sequences.get(0);
        }
        
        if (sequence == null)
        {
            // no hope...
            dataSetName = "--";
        }
        else
        {
            // replace the sheet name by the file or sequence name
            dataSetName = FileUtil.getFileName(sequence.getFilename());
            if (dataSetName.isEmpty()) dataSetName = sequence.getName();
        }
        
        // make the name "safe"
        return WorkbookUtil.createSafeSheetName(dataSetName);
    }
    
    String getSheetName(Sequence sequence)
    {
        String sheetName = "ROI Statistics";
        if (sequence != null && !isHeadLess()) sheetName = getDataSetName(sequence);
        return sheetName;
    }
    
    private void updateStatistics()
    {
        if (isHeadLess())
        {
            updateStatistics(sequence.getValue());
        }
        else for (Sequence sequence : getSequences())
        {
            updateStatistics(sequence);
        }
    }
    
    private void updateStatistics(final Sequence sequence)
    {
        final Workbook wb = book.getValue();
        
        String sheetName = getSheetName(sequence);
        
        final Sheet sheet;
        Row header = null;
        
        if (wb.getSheet(sheetName) != null)
        {
            sheet = wb.getSheet(sheetName);
        }
        else
        {
            // create the sheet
            sheet = wb.createSheet(sheetName);
            measureSelectionChanged = true;
        }
        
        if (measureSelectionChanged)
        {
            measureSelectionChanged = false;
            header = sheet.createRow(0);
            
            // create the header row
            Measures[] measures = Measures.values();
            
            int nbIntensityStats = 0;
            for (Measures measure : measures)
                if (measure.name().startsWith("INTENSITY") && measure.isSelected()) nbIntensityStats++;
            
            for (Measures measure : measures)
            {
                if (!measure.isSelected()) continue;
                
                int col = measure.getColumnIndex();
                String name = measure.toString();
                
                if (measure.name().startsWith("INTENSITY") && sequence != null)
                {
                    for (int c = 0; c < sequence.getSizeC(); c++)
                    {
                        header.getCell(col + nbIntensityStats * c).setCellValue(name + sequence.getChannelName(c));
                    }
                }
                else
                {
                    header.getCell(col).setCellValue(name);
                }
            }
        }
        
        final List<ROI> rois2Update = isHeadLess() ? Arrays.asList(this.rois.getValue()) : sequence.getROIs(true);
        
        Runnable fullUpdate = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    IcySpreadSheet icySheet = new IcySpreadSheet(sheet);
                    
                    int rowID = 1;
                    icySheet.removeRows(rowID);
                    
                    List<Future<List<Object>>> results = new ArrayList<Future<List<Object>>>(rois2Update.size());
                    
                    for (ROI roi : rois2Update)
                    {
                        if (cpus.isShutdown() || cpus.isTerminating()) return;
                        results.add(cpus.submit(createUpdater(sequence, roi)));
                    }
                    
                    for (Future<List<Object>> result : results)
                        updateWorkbook(wb, icySheet, rowID++, result.get(), false);
                }
                catch (InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                    return;
                }
                catch (Exception e)
                {
                    return;
                }
                
                book.valueChanged(book, null, book.getValue());
            }
        };
        
        if (isHeadLess())
        {
            fullUpdate.run();
        }
        else
        {
            ThreadUtil.bgRun(fullUpdate);
        }
    }
    
    private void updateStatistics(ROI roi)
    {
        Workbook wb = book.getValue();
        
        for (Sequence sequence : roi.getSequences())
            if (Icy.getMainInterface().isOpened(sequence)) try
            {
                IcySpreadSheet sheet = Workbooks.getSheet(wb, getSheetName(sequence));
                
                int rowID = sequence.getROIs(true).indexOf(roi) + 1;
                {
                    List<Object> measures = createUpdater(sequence, roi).call();
                    updateWorkbook(wb, sheet, rowID, measures, true);
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
    }
    
    private void updateWorkbook(Workbook wb, IcySpreadSheet sheet, int rowID, List<Object> measures, boolean updateInterface)
    {
        for (int colID = 0; colID < measures.size(); colID++)
        {
            Object value = measures.get(colID);
            
            if (value instanceof Color)
            {
                sheet.setFillColor(rowID, colID, (Color) value);
            }
            else sheet.setValue(rowID, colID, value);
        }
        
        if (updateInterface) book.valueChanged(book, null, book.getValue());
    }
    
    private Callable<List<Object>> createUpdater(final Sequence sequence, final ROI roi2Update)
    {
        return new Callable<List<Object>>()
        {
            @Override
            public List<Object> call()
            {
                List<Object> measures = new ArrayList<Object>();
                
                ROI roi = roi2Update;
                
                if (roi instanceof ROI2D && !(roi instanceof ROI2DArea))
                {
                    ROI2D r2 = (ROI2D) roi;
                    ROI2DArea area = new ROI2DArea(r2.getBooleanMask(false));
                    area.setZ(r2.getZ());
                    area.setT(r2.getT());
                    area.setName(roi.getName());
                    area.setColor(roi.getColor());
                    roi = area;
                }
                else if (roi instanceof ROI3D && !(roi instanceof ROI3DArea))
                {
                    ROI3D r3 = (ROI3D) roi;
                    ROI3DArea area = new ROI3DArea(r3.getBooleanMask(false));
                    area.setT(r3.getT());
                    area.setName(roi.getName());
                    area.setColor(roi.getColor());
                    roi = area;
                }
                
                if (Thread.currentThread().isInterrupted()) return measures;
                
                if (Measures.FULLPATH.isSelected())
                {
                    String fullPath = "--";
                    if (sequence != null && sequence.getFilename() != null)
                    {
                        fullPath = FileUtil.getDirectory(sequence.getFilename(), false);
                    }
                    measures.add(fullPath);
                }
                if (Measures.FOLDER.isSelected())
                {
                    String folder = "--";
                    if (sequence != null && sequence.getFilename() != null)
                    {
                        folder = FileUtil.getDirectory(sequence.getFilename(), false);
                        folder = folder.substring(1 + folder.lastIndexOf(File.separator));
                    }
                    measures.add(folder);
                }
                if (Measures.DATASET.isSelected())
                {
                    measures.add(getDataSetName(sequence));
                }
                if (Measures.NAME.isSelected())
                {
                    measures.add(roi.getName());
                }
                if (Measures.COLOR.isSelected())
                {
                    measures.add(roi.getColor());
                }
                
                // POSITION (MASS CENTER)
                
                Point5D center = ROIMassCenterDescriptorsPlugin.computeMassCenter(roi);
                
                if (Measures.X.isSelected())
                {
                    measures.add(center.getX());
                }
                if (Measures.Y.isSelected())
                {
                    measures.add(center.getY());
                }
                if (Measures.Z.isSelected())
                {
                    measures.add(roi instanceof ROI2D && center.getZ() == -1 ? "ALL" : center.getZ());
                }
                if (Measures.T.isSelected())
                {
                    measures.add(center.getT() == -1 ? "ALL" : center.getT());
                }
                if (Measures.C.isSelected())
                {
                    measures.add(center.getC() == -1 ? "ALL" : center.getC());
                }
                
                // BOUNDS
                
                if (Measures.BOX_WIDTH.isSelected() || Measures.BOX_HEIGHT.isSelected() || Measures.BOX_DEPTH.isSelected())
                {
                    Rectangle5D bounds5 = roi.getBounds5D();
                    
                    if (Measures.BOX_WIDTH.isSelected())
                    {
                        measures.add(bounds5.getSizeX());
                    }
                    if (Measures.BOX_HEIGHT.isSelected())
                    {
                        measures.add(bounds5.getSizeY());
                    }
                    if (Measures.BOX_DEPTH.isSelected())
                    {
                        measures.add(roi instanceof ROI3D ? bounds5.getSizeZ() : "N/A");
                    }
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                
                // CONTOUR & INTERIOR
                
                if (Measures.CONTOUR.isSelected())
                {
                    measures.add(roi.getNumberOfContourPoints());
                }
                if (Measures.INTERIOR.isSelected())
                {
                    measures.add(roi.getNumberOfPoints());
                }
                
                if (Thread.currentThread().isInterrupted()) return measures;
                
                // SHAPE MEASURES
                
                if (Measures.SPHERICITY.isSelected())
                {
                    measures.add(computeSphericity(roi));
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                if (Measures.ROUNDNESS.isSelected())
                {
                    measures.add(roi.getNumberOfPoints() == 1 ? 100 : computeRoundness(roi));
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                if (Measures.CONVEXITY.isSelected())
                {
                    measures.add(computeConvexity(roi));
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                if (Measures.MAX_FERET.isSelected())
                {
                    measures.add(computeMaxFeret(roi));
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                
                // ELLIPSE FITTING
                
                if (Measures.ELLIPSE_A.isSelected() || Measures.ELLIPSE_B.isSelected() || Measures.ELLIPSE_C.isSelected() || Measures.YAW.isSelected()
                        || Measures.PITCH.isSelected() || Measures.ROLL.isSelected() || Measures.ELONGATION.isSelected() || Measures.FLATNESS3D.isSelected())
                {
                    double[] ellipse = ROIEllipsoidFittingDescriptor.computeOrientation(roi, null);
                    
                    if (Measures.ELLIPSE_A.isSelected())
                    {
                        measures.add(ellipse != null ? ellipse[0] : "N/A");
                    }
                    if (Measures.ELLIPSE_B.isSelected())
                    {
                        measures.add(ellipse != null ? ellipse[1] : "N/A");
                    }
                    if (Measures.ELLIPSE_C.isSelected())
                    {
                        measures.add(roi instanceof ROI3D && ellipse != null ? ellipse[2] : "N/A");
                    }
                    if (Measures.YAW.isSelected())
                    {
                        measures.add(Math.toDegrees(Math.acos(-ellipse[3])));
                    }
                    if (Measures.PITCH.isSelected())
                    {
                        measures.add(Math.toDegrees(Math.asin(ellipse[5])));
                    }
                    if (Measures.ROLL.isSelected())
                    {
                        measures.add(Math.toDegrees(Math.atan2(ellipse[8], ellipse[11])));
                    }
                    if (Measures.ELONGATION.isSelected())
                    {
                        measures.add(ellipse != null && ellipse[1] != 0 ? ellipse[0] / ellipse[1] : "N/A");
                    }
                    if (Measures.FLATNESS3D.isSelected())
                    {
                        measures.add(roi instanceof ROI3D && ellipse != null && ellipse[2] != 0 ? ellipse[1] / ellipse[2] : "N/A");
                    }
                    if (Thread.currentThread().isInterrupted()) return measures;
                }
                
                // INTENSITY STATISTICS
                
                // how many measures are selected?
                int nbIntensityStats = 0, nbTextureStats = 0;
                for (Measures measure : Measures.values())
                {
                    if (measure.name().startsWith("INTENSITY") && measure.isSelected())
                    {
                        nbIntensityStats++;
                        
                        if (measure.name().contains("TEXTURE"))
                        {
                            nbTextureStats++;
                        }
                    }
                }
                
                if (nbIntensityStats > 0 && sequence != null && !(roi instanceof ROI5D))
                {
                    for (int c = 0; c < sequence.getSizeC(); c++)
                    {
                        if (Thread.currentThread().isInterrupted()) return measures;
                        
                        SequenceDataIterator iterator = new SequenceDataIterator(sequence, roi, false, -1, -1, c);
                        
                        // minimum / maximum / sum
                        double min = Double.MAX_VALUE, max = 0, sum = 0, cpt = 0;
                        while (!iterator.done())
                        {
                            double val = iterator.get();
                            if (val > max) max = val;
                            if (val < min) min = val;
                            sum += val;
                            cpt++;
                            iterator.next();
                        }
                        double avg = sum / cpt;
                        
                        double std = 0;
                        if (Measures.INTENSITY_STD.isSelected())
                        {
                            iterator.reset();
                            while (!iterator.done())
                            {
                                double dev = iterator.get() - avg;
                                std += dev * dev;
                                iterator.next();
                            }
                            std = Math.sqrt(std / cpt);
                        }
                        
                        if (Measures.INTENSITY_MIN.isSelected())
                        {
                            measures.add(min);
                        }
                        if (Measures.INTENSITY_AVG.isSelected())
                        {
                            measures.add(avg);
                        }
                        if (Measures.INTENSITY_MAX.isSelected())
                        {
                            measures.add(max);
                        }
                        if (Measures.INTENSITY_SUM.isSelected())
                        {
                            measures.add(sum);
                        }
                        if (Measures.INTENSITY_STD.isSelected())
                        {
                            measures.add(std);
                        }
                        
                        // Texture
                        
                        Map<ROIDescriptor, Object> map = null;
                        
                        try
                        {
                            if (nbTextureStats > 0 && roi instanceof ROI2D)
                            {
                                ROI2D r2 = (ROI2D) roi.getCopy();
                                r2.setC(c);
                                map = new ROIHaralickTextureDescriptor().compute(r2, sequence);
                            }
                        }
                        catch (UnsupportedOperationException u)
                        {
                            
                        }
                        
                        if (Measures.INTENSITY_TEXTURE_ASM.isSelected())
                        {
                            measures.add(map == null ? "N.A." : map.get(ROIHaralickTextureDescriptor.angularSecondMoment));
                        }
                        if (Measures.INTENSITY_TEXTURE_CONT.isSelected())
                        {
                            measures.add(map == null ? "N.A." : map.get(ROIHaralickTextureDescriptor.contrast));
                        }
                        if (Measures.INTENSITY_TEXTURE_ENT.isSelected())
                        {
                            measures.add(map == null ? "N.A." : map.get(ROIHaralickTextureDescriptor.entropy));
                        }
                        if (Measures.INTENSITY_TEXTURE_HOMO.isSelected())
                        {
                            measures.add(map == null ? "N.A." : map.get(ROIHaralickTextureDescriptor.homogeneity));
                        }
                    }
                }
                
                return measures;
            }
        };
    }
    
    /**
     * @param roi
     * @return An array containing the [min, max] diameters of the best fitting ellipse for the
     *         specified ROI
     * @deprecated This method should not be used directly and will be removed in future releases in
     *             favor of a {@link ROIDescriptor} implementation
     */
    public static double[] computeEllipseDimensions(ROI roi)
    {
        double[] ellipse = ROIEllipsoidFittingDescriptor.computeOrientation(roi, null);
        
        return new double[] { ellipse[0], ellipse[1], ellipse[2] };
    }
    
    /**
     * The sphericity is the normalised ratio between the perimeter and area of a ROI (in 2D), or
     * its volume and surface area (in 3D).
     * 
     * @param roi
     *            the input component
     * @return 100% for a perfect circle (or sphere), and lower otherwise
     * @deprecated Use {@link ROISphericity#computeSphericity(ROI)} instead
     */
    public static double computeSphericity(ROI roi)
    {
        return ROISphericity.computeSphericity(roi);
    }
    
    /**
     * The roundness is approximated here by the ratio between the smallest and largest distance
     * from the mass center to any point on the surface, and expressed as a percentage.
     * 
     * @return 100% for a perfect circle (or sphere in 3D), and lower otherwise
     * @deprecated Use {@link ROIRoundness#computeRoundness(ROI)} instead
     */
    public static double computeRoundness(ROI roi)
    {
        return ROIRoundness.computeRoundness(roi);
    }
    
    /**
     * @param roi
     * @return The maximum Feret diameter (i.e. the longest distance between any 2 points of the
     *         ROI)
     * @deprecated Use {@link ROIFeretDiameter#computeFeretDiameter(ROI, Sequence)} instead
     */
    public static double computeMaxFeret(ROI roi)
    {
        return ROIFeretDiameter.computeFeretDiameter(roi, null);
    }
    
    /**
     * @param roi
     * @return The hull ratio, measured as the ratio between the object volume and its convex hull
     *         (envelope)
     * @deprecated Use {@link ROIConvexity#computeConvexity(ROI)} instead
     */
    public static double computeConvexity(ROI roi)
    {
        return ROIConvexity.computeConvexity(roi);
    }
    
    /**
     * @param roi
     * @return An array containing [contour, area] of the smallest convex envelope surrounding the
     *         object. The 2 values are returned together because their computation is simultaneous
     * @deprecated use {@link Convexify#createConvexROI(ROI)} instead
     */
    public static double[] computeHullDimensions(ROI roi)
    {
        ROI convexHull = Convexify.createConvexROI(roi);
        
        return new double[] { convexHull.getNumberOfContourPoints(), convexHull.getNumberOfPoints() };
    }
    
    // PUBLIC STATIC METHODS (useful for script access) //
    
    /**
     * @param roi
     * @return
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)} instead.
     */
    public static Point5D getMassCenter(ROI roi)
    {
        return ROIMassCenterDescriptorsPlugin.computeMassCenter(roi);
    }
    
    /**
     * @param roi
     * @return
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)} instead.
     */
    public static Point2D getMassCenter(ROI2D roi)
    {
        return getMassCenter((ROI) roi).toPoint2D();
    }
    
    /**
     * @param roi
     * @return
     * @deprecated use {@link ROIMassCenterDescriptorsPlugin#computeMassCenter(ROI)} instead.
     */
    public static Point3D getMassCenter(ROI3D roi)
    {
        return getMassCenter((ROI) roi).toPoint3D();
    }
    
    // MAIN LISTENER //
    
    @Override
    public void sequenceOpened(Sequence sequence)
    {
        sequence.addListener(this);
        for (ROI roi : sequence.getROIs())
            roi.addListener(this);
        
        // update(null);
        updateStatistics(sequence);
        
        // this is mandatory since sheet creation cannot be detected
        book.valueChanged(book, null, book.getValue());
    }
    
    @Override
    public void sequenceClosed(Sequence sequence)
    {
        sequence.removeListener(this);
        for (ROI roi : sequence.getROIs())
            roi.removeListener(this);
        
        String sheetName = getSheetName(sequence);
        
        int index = book.getValue().getSheetIndex(sheetName);
        
        if (index >= 0)
        {
            book.getValue().removeSheetAt(index);
            // this is mandatory since sheet creation cannot be detected
            book.valueChanged(book, null, book.getValue());
        }
    }
    
    // SEQUENCE LISTENER //
    
    @Override
    public void sequenceChanged(SequenceEvent sequenceEvent)
    {
        if (sequenceEvent.getSourceType() == SequenceEventSourceType.SEQUENCE_DATA)
        {
            updateStatistics(sequenceEvent.getSequence());
        }
        else if (sequenceEvent.getSourceType() == SequenceEventSourceType.SEQUENCE_ROI)
        {
            ROI roi = (ROI) sequenceEvent.getSource();
            
            switch (sequenceEvent.getType())
            {
            case ADDED:
                
                if (roi == null)
                {
                    // multiple ROI were added
                    updateStatistics();
                }
                else
                {
                    for (Sequence sequence : roi.getSequences())
                        updateStatistics(sequence);
                    // remove it first to avoid duplicates
                    roi.removeListener(this);
                    roi.addListener(this);
                }
                break;
            
            case REMOVED:
                
                if (roi == null)
                {
                    // multiple ROIs have been removed
                    System.err.println("[ROI Statistics] Warning: potential memory leak");
                }
                else
                {
                    roi.removeListener(this);
                }
                updateStatistics();
                break;
            
            case CHANGED: // don't do anything, roiChanged() will do this for us
            }
        }
    }
    
    @Override
    public void clean()
    {
        // remove listeners
        for (final Sequence sequence : Icy.getMainInterface().getSequences())
            sequence.removeListener(this);
        
        Icy.getMainInterface().removeGlobalSequenceListener(this);
        
        cpus.shutdownNow();
    }
    
    @Override
    public void roiChanged(final ROIEvent event)
    {
        if (event.getType() == ROIEventType.ROI_CHANGED
                || event.getType() == ROIEventType.PROPERTY_CHANGED && (event.getPropertyName().equalsIgnoreCase("name") || event.getPropertyName().equalsIgnoreCase("color")))
        {
            updateStatistics(event.getSource());
            // this is mandatory since sheet modification cannot be detected
            book.valueChanged(book, null, book.getValue());
        }
    }
    
    private class MeasureSelector implements TriggerListener
    {
        final EzDialog selector;
        
        public MeasureSelector()
        {
            selector = new EzDialog("Select measures...");
            
            selector.setLayout(new BoxLayout(selector.getContentPane(), BoxLayout.Y_AXIS));
            
            for (final Measures measure : Measures.values())
            {
                String name = measure.toString();
                
                if (measure.name().startsWith("INTENSITY"))
                {
                    if (measure.name().contains("TEXTURE"))
                    {
                        name += "(texture)";
                    }
                    else
                    {
                        name += "intensity";
                    }
                }
                
                JCheckBox check = new JCheckBox(name, measure.isSelected());
                check.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent arg0)
                    {
                        measure.toggleSelection();
                        measureSelectionChanged = true;
                    }
                });
                selector.addComponent(check);
                if (measure == Measures.BOX_DEPTH || measure == Measures.FLATNESS3D)
                {
                    selector.addComponent(new JSeparator(JSeparator.VERTICAL));
                }
            }
        }
        
        @Override
        public void valueChanged(Var<Integer> source, Integer oldValue, Integer newValue)
        {
        }
        
        @Override
        public void referenceChanged(Var<Integer> source, Var<? extends Integer> oldReference, Var<? extends Integer> newReference)
        {
        }
        
        @Override
        public void triggered(VarTrigger source)
        {
            selector.pack();
            selector.showDialog(true);
            if (measureSelectionChanged)
            {
                // save settings
                Measures.saveSelectionToPreferences();
                
                book.setValue(null);
                
                if (!isHeadLess()) execute();
            }
        }
        
    }
}
