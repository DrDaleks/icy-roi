package plugins.adufour.roi;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.Clusterer;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.EuclideanDistance;

import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.type.point.Point3D;
import plugins.adufour.ezplug.EzLabel;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi3d.ROI3DArea;

public class SplitROI extends EzPlug
{
    private enum Mode
    {
        MANUAL("Splits the selected ROI into a given number of components"), AUTO_SIZE("Compute the median ROI size and split\nROI as many times as this median size fits"),;
        
        final String desc;
        
        Mode(String description)
        {
            desc = description;
        }
    }
    
    EzVarSequence sequence = new EzVarSequence("Select ROI from");
    
    EzVarEnum<Mode> mode = new EzVarEnum<Mode>("Split mode", Mode.values());
    
    EzLabel description = new EzLabel(mode.getValue().desc);
    
    EzVarInteger nbComponents = new EzVarInteger("Number of components", 2, 2, 10, 1);
    
    @Override
    public void clean()
    {
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(sequence);
        addEzComponent(mode);
        addEzComponent(description);
        mode.addVarChangeListener(new EzVarListener<SplitROI.Mode>()
        {
            @Override
            public void variableChanged(EzVar<Mode> source, Mode newValue)
            {
                description.setText(newValue.desc);
            }
        });
        addEzComponent(nbComponents);
        mode.addVisibilityTriggerTo(nbComponents, Mode.MANUAL);
    }
    
    @Override
    protected void execute()
    {
        Sequence s = sequence.getValue(true);
        
        if (mode.getValue() == Mode.AUTO_SIZE)
        {
            ROI[] newROI = SplitROIByMedianSize.splitByMedianSize(s.getROIs());
            s.removeAllROI(true);
            s.addROIs(Arrays.asList(newROI), true);
        }
        else if (mode.getValue() == Mode.MANUAL)
        {
            for (ROI roi : s.getSelectedROIs())
            {
                ROI[] newROI = split(roi, nbComponents.getValue());
                s.removeROI(roi, true);
                s.addROIs(Arrays.asList(newROI), true);
            }
        }
    }
    
    public static ROI[] split(ROI roi, int nbObjects)
    {
        if (roi instanceof ROI2D) return split((ROI2D) roi, nbObjects);
        else if (roi instanceof ROI3D) return split((ROI3D) roi, nbObjects);
        
        throw new IcyHandledException("Error: cannot split ROI that are neither 2D nor 3D");
    }
    
    /**
     * Splits the specified 2D ROI into the specified number of parts, based on a basic distance
     * clustering approach
     * 
     * @param roi
     * @param nbObjects
     * @return
     */
    public static ROI2D[] split(final ROI2D roi, int nbObjects)
    {
        ROI2D[] newROI = new ROI2D[nbObjects];
        
        ArrayList<Clusterable> data = new ArrayList<Clusterable>();
        
        // populate the data set with pixel coordinates
        for (final Point pt : roi.getBooleanMask(true).getPoints())
            data.add(new Clusterable()
            {
                @Override
                public double[] getPoint()
                {
                    return new double[] { pt.x, pt.y };
                }
            });
        
        // Load the clustering algorithm
        Clusterer<Clusterable> clusterer;
        clusterer = new KMeansPlusPlusClusterer<Clusterable>(nbObjects, 1000, new EuclideanDistance());
        
        // Cluster the data and create a new ROI for each cluster
        int clusterID = 0;
        for (Cluster<Clusterable> cluster : clusterer.cluster(data))
        {
            ROI2DArea area = new ROI2DArea();
            area.setT(roi.getT());
            area.setC(roi.getC());
            area.setZ(roi.getZ());
            area.setColor(roi.getColor());
            
            for (Clusterable pt : cluster.getPoints())
            {
                double[] xy = pt.getPoint();
                area.addPoint((int) xy[0], (int) xy[1]);
            }
            newROI[clusterID++] = area;
        }
        
        return isSplitValid(roi, newROI) ? newROI : new ROI2D[] { roi };
    }
    
    /**
     * Splits the specified 3D ROI into the specified number of parts, based on a basic distance
     * clustering approach
     * 
     * @param roi
     * @param nbObjects
     * @return
     */
    public static ROI3D[] split(ROI3D roi, int nbObjects)
    {
        ROI3D[] newROI = new ROI3D[nbObjects];
        
        ArrayList<Clusterable> data = new ArrayList<Clusterable>();
        
        // populate the data set with pixel coordinates
        for (final Point3D.Integer pt : roi.getBooleanMask(true).getPoints())
            data.add(new Clusterable()
            {
                @Override
                public double[] getPoint()
                {
                    return new double[] { pt.x, pt.y, pt.z };
                }
            });
        
        // Load the clustering algorithm
        Clusterer<Clusterable> clusterer = new KMeansPlusPlusClusterer<Clusterable>(nbObjects);
        
        // Cluster the data and create a new ROI for each cluster
        int clusterID = 0;
        for (Cluster<Clusterable> cluster : clusterer.cluster(data))
        {
            ROI3DArea area = new ROI3DArea();
            area.setT(roi.getT());
            area.setC(roi.getC());
            area.setColor(roi.getColor());
            
            for (Clusterable pt : cluster.getPoints())
            {
                double[] xyz = pt.getPoint();
                area.addPoint((int) xyz[0], (int) xyz[1], (int) xyz[2]);
            }
            newROI[clusterID++] = area;
        }
        
        return isSplitValid(roi, newROI) ? newROI : new ROI3D[] { roi };
    }
    
    /**
     * Validates (or invalidates) a split using a heuristic rule that checks whether the union of
     * all splits is smaller than the initial ROI's convex envelope
     * 
     * @param input
     * @param output
     */
    private static boolean isSplitValid(ROI input, ROI[] splits)
    {
        // Check if the split is valid based on convexity
        double hullSize = Convexify.createConvexROI(input).getNumberOfPoints();
        double newSize = 0;
        for (ROI split : splits)
            newSize += split.getNumberOfPoints();
        
        // Heuristic check
        return (newSize < hullSize * 0.97);
    }
}
