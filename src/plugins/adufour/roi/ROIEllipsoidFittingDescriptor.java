package plugins.adufour.roi;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.SingularMatrixException;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import icy.math.MathUtil;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.point.Point3D;
import plugins.adufour.vars.lang.VarDouble;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;

public class ROIEllipsoidFittingDescriptor extends Plugin implements PluginROIDescriptor, PluginBundled
{
    public static class ROIFirstDiameter extends ROIDescriptor
    {
        protected ROIFirstDiameter()
        {
            super("1st Diameter", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Diameter of the best fitting ellipse along the first principle axis</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeFirstDiameter(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The diameter of the best fitting ellipse along the first principle axis
         */
        public static double computeFirstDiameter(ROI roi, Sequence sequence)
        {
            return computeOrientation(roi, sequence)[0];
        }
    }
    
    public static class ROISecondDiameter extends ROIDescriptor
    {
        protected ROISecondDiameter()
        {
            super("2nd Diameter", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Diameter of the best fitting ellipse along the second principle axis</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeSecondDiameter(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The diameter of the best fitting ellipse along the second principle axis
         */
        public static double computeSecondDiameter(ROI roi, Sequence sequence)
        {
            return computeOrientation(roi, sequence)[1];
        }
    }
    
    public static class ROIThirdDiameter extends ROIDescriptor
    {
        protected ROIThirdDiameter()
        {
            super("3rd Diameter", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Diameter of the best fitting ellipse along the third principle axis (0 in 2D)</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeThirdDiameter(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The diameter of the best fitting ellipse along the third principle axis (0 in 2D)
         */
        public static double computeThirdDiameter(ROI roi, Sequence sequence)
        {
            if (roi instanceof ROI2D) return 0;
            
            return computeOrientation(roi, sequence)[2];
        }
    }
    
    public static class ROIFirstAxis extends ROIDescriptor
    {
        protected ROIFirstAxis()
        {
            super("1st axis", Vector3d.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>First principle axis of the best fitting ellipse</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeFirstAxis(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The first (or major) principle axis of the best fitting ellipse
         */
        public static Vector3d computeFirstAxis(ROI roi, Sequence sequence)
        {
            double[] ellipse = computeOrientation(roi, sequence);
            return new Vector3d(ellipse[3], ellipse[4], ellipse[5]);
        }
    }
    
    public static class ROISecondAxis extends ROIDescriptor
    {
        protected ROISecondAxis()
        {
            super("2nd axis", Vector3d.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Second principle axis of the best fitting ellipse</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeSecondAxis(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The second principle axis of the best fitting ellipse
         */
        public static Vector3d computeSecondAxis(ROI roi, Sequence sequence)
        {
            double[] ellipse = computeOrientation(roi, sequence);
            return new Vector3d(ellipse[6], ellipse[7], ellipse[8]);
        }
    }
    
    public static class ROIThirdAxis extends ROIDescriptor
    {
        protected ROIThirdAxis()
        {
            super("3rd axis", Vector3d.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Third principle axis of the best fitting ellipse</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return ROIEllipsoidFittingDescriptor.getUnit(sequence);
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeThirdAxis(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The third principle axis of the best fitting ellipse
         */
        public static Vector3d computeThirdAxis(ROI roi, Sequence sequence)
        {
            if (roi instanceof ROI2D) return new Vector3d();
            
            double[] ellipse = computeOrientation(roi, sequence);
            return new Vector3d(ellipse[9], ellipse[10], ellipse[11]);
        }
    }
    
    public static class ROIYawAngle extends ROIDescriptor
    {
        protected ROIYawAngle()
        {
            super("Yaw", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Yaw angle (counter-clockwise, 0 aligns with the X axis)</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return "\u00B0";
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeYawAngle(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The yaw angle in degrees in the X-Y plane (counter-clockwise away from the X
         *         axis)
         */
        public static double computeYawAngle(ROI roi, Sequence sequence)
        {
            double[] ellipse = computeOrientation(roi, sequence);
            // Yaw: project onto the X axis and compute the arc-cosine
            return Math.toDegrees(Math.acos(-ellipse[3]));
        }
    }
    
    public static class ROIPitchAngle extends ROIDescriptor
    {
        protected ROIPitchAngle()
        {
            super("Pitch", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Pitch angle (0 aligns with the X-Y plane, the sign follows the Z axis)</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return "\u00B0";
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computePitchAngle(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The pitch angle in degrees (0 aligns with the X-Y plane, the sign follows the Z
         *         axis), or 0 if the ROI is 2D
         */
        public static double computePitchAngle(ROI roi, Sequence sequence)
        {
            if (roi instanceof ROI2D) return 0;
            
            double[] ellipse = computeOrientation(roi, sequence);
            // Pitch: project onto the Z axis and compute the arc-sine
            return Math.toDegrees(Math.asin(ellipse[5]));
        }
    }
    
    public static class ROIRollAngle extends ROIDescriptor
    {
        protected ROIRollAngle()
        {
            super("Roll", Double.class);
        }
        
        @Override
        public String getDescription()
        {
            return "<html>Roll angle (counter-clockwise rotation around its first principal axis)</html>";
        }
        
        @Override
        public String getUnit(Sequence sequence)
        {
            return "\u00B0";
        }
        
        @Override
        public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
        {
            return computeRollAngle(roi, sequence);
        }
        
        /**
         * WARNING: calling this method is not optimal. To calculate all ellipse parameters at once,
         * use {@link ROIEllipsoidFittingDescriptor#computeOrientation(ROI)}.
         * 
         * @param roi
         * @return The roll angle in degrees (counter-clockwise around its first principal axis), or
         *         0 if the ROI is 2D
         */
        public static double computeRollAngle(ROI roi, Sequence sequence)
        {
            if (roi instanceof ROI2D) return 0;
            
            double[] ellipse = computeOrientation(roi, sequence);
            // roll: angle between the 2nd principal axis and the XY plane
            // = arctan2(secondAxis.z, thirdAxis.z)
            return Math.toDegrees(Math.atan2(ellipse[8], ellipse[11]));
        }
    }
    
    private static final ROIFirstDiameter  majorDiameter   = new ROIFirstDiameter();
    private static final ROISecondDiameter minorDiameter2D = new ROISecondDiameter();
    private static final ROIThirdDiameter  minorDiameter3D = new ROIThirdDiameter();
    private static final ROIFirstAxis      majorAxis       = new ROIFirstAxis();
    private static final ROISecondAxis     minorAxis2D     = new ROISecondAxis();
    private static final ROIThirdAxis      minorAxis3D     = new ROIThirdAxis();
    private static final ROIYawAngle       yawAngle        = new ROIYawAngle();
    private static final ROIPitchAngle     pitchAngle      = new ROIPitchAngle();
    private static final ROIRollAngle      rollAngle       = new ROIRollAngle();
    
    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        ArrayList<ROIDescriptor> list = new ArrayList<ROIDescriptor>();
        
        list.add(majorDiameter);
        list.add(minorDiameter2D);
        list.add(minorDiameter3D);
        list.add(majorAxis);
        list.add(minorAxis2D);
        list.add(minorAxis3D);
        list.add(yawAngle);
        list.add(pitchAngle);
        list.add(rollAngle);
        
        return list;
    }
    
    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
    {
        double[] ellipse = computeOrientation(roi, sequence);
        
        HashMap<ROIDescriptor, Object> map = new HashMap<ROIDescriptor, Object>(6);
        
        map.put(majorDiameter, ellipse[0]);
        map.put(minorDiameter2D, ellipse[1]);
        map.put(minorDiameter3D, ellipse[2]);
        Vector3d axis1 = new Vector3d(ellipse[3], ellipse[4], ellipse[5]);
        Vector3d axis2 = new Vector3d(ellipse[6], ellipse[7], ellipse[8]);
        Vector3d axis3 = new Vector3d(ellipse[9], ellipse[10], ellipse[11]);
        map.put(majorAxis, axis1);
        map.put(minorAxis2D, axis2);
        map.put(minorAxis3D, axis3);
        map.put(yawAngle, Math.toDegrees(Math.acos(-ellipse[3])));
        map.put(pitchAngle, roi instanceof ROI2D ? 0 : Math.toDegrees(Math.asin(ellipse[5])));
        map.put(rollAngle, roi instanceof ROI2D ? 0 : Math.toDegrees(Math.atan2(ellipse[8], ellipse[11])));
        
        return map;
    }
    
    private static String getUnit(Sequence sequence)
    {
        if (sequence == null) return "px";
        int dim = sequence.getSizeZ() > 1 ? 3 : 2;
        return sequence.getBestPixelSizeUnit(dim, 1).toString();
    }
    
    /**
     * @param roi
     * @return A 12-value array describing the ellipse fit as follows:<br/>
     *         <ul>
     *         <li>index 0: diameter along the first principle axis</li>
     *         <li>index 1: diameter along the second principle axis</li>
     *         <li>index 2: diameter along the third principle axis (0 in 2D)</li>
     *         <li>index 3: the X component of the first principle axis vector</li>
     *         <li>index 4: the Y component of the first principle axis vector</li>
     *         <li>index 5: the Z component of the first principle axis vector (0 in 2D)</li>
     *         <li>index 6: the X component of the second principle axis vector</li>
     *         <li>index 7: the Y component of the second principle axis vector</li>
     *         <li>index 8: the Z component of the second principle axis vector (0 in 2D)</li>
     *         <li>index 9: the X component of the third principle axis vector (0 in 2D)</li>
     *         <li>index 10: the Y component of the third principle axis vector (0 in 2D)</li>
     *         <li>index 11: the Z component of the third principle axis vector (1 in 2D)</li>
     *         </ul>
     */
    public static double[] computeOrientation(ROI roi, Sequence sequence)
    {
        double[] ellipse = new double[12];
        
        if (roi instanceof ROI2D)
        {
            try
            {
                Point2d radii = new Point2d();
                Vector2d[] eigenVectors = new Vector2d[2];
                fitEllipse((ROI2D) roi, null, radii, null, eigenVectors, null);
                
                // convert from radius to diameter
                radii.scale(2.0);
                
                // diameters
                ellipse[0] = radii.x;
                ellipse[1] = radii.y;
                
                // vectors
                Vector2d firstAxis = eigenVectors[0];
                ellipse[3] = MathUtil.round(firstAxis.x, 2);
                ellipse[4] = MathUtil.round(firstAxis.y, 2);
                Vector2d secondAxis = eigenVectors[1];
                ellipse[6] = MathUtil.round(secondAxis.x, 2);
                ellipse[7] = MathUtil.round(secondAxis.y, 2);
                // 3rd axis is always normal to the XY plane
                ellipse[11] = 1.0;
            }
            catch (RuntimeException e)
            {
                System.err.println("Warning: could not fit ellipse on ROI \"" + roi.getName() + "\": " + e.getMessage());
                Arrays.fill(ellipse, Double.NaN);
            }
        }
        else if (roi instanceof ROI3D)
        {
            Point3d radii = new Point3d();
            Vector3d[] eigenVectors = new Vector3d[3];
            fitEllipse((ROI3D) roi, null, radii, eigenVectors, null);
            
            // convert from radius to diameter
            radii.scale(2.0);
            
            // diameters
            ellipse[0] = radii.x;
            ellipse[1] = radii.y;
            ellipse[2] = radii.z;
            
            // vectors
            Vector3d firstAxis = eigenVectors[0];
            if (firstAxis != null)
            {
                ellipse[3] = MathUtil.round(firstAxis.x, 2);
                ellipse[4] = MathUtil.round(firstAxis.y, 2);
                ellipse[5] = MathUtil.round(firstAxis.z, 2);
            }
            Vector3d secondAxis = eigenVectors[1];
            if (secondAxis != null)
            {
                ellipse[6] = MathUtil.round(secondAxis.x, 2);
                ellipse[7] = MathUtil.round(secondAxis.y, 2);
                ellipse[8] = MathUtil.round(secondAxis.z, 2);
            }
            Vector3d thirdAxis = eigenVectors[2];
            if (thirdAxis != null)
            {
                ellipse[9] = MathUtil.round(thirdAxis.x, 2);
                ellipse[10] = MathUtil.round(thirdAxis.y, 2);
                ellipse[11] = MathUtil.round(thirdAxis.z, 2);
            }
        }
        else
        {
            System.err.println("Cannot compute ellipse dimensions for ROI of type: " + roi.getClassName());
            Arrays.fill(ellipse, Double.NaN);
        }
        
        Arrays.sort(ellipse, 0, 3);
        // sorting is in ascending order, but we need the largest diameter first
        double tmp = ellipse[0];
        ellipse[0] = ellipse[2];
        ellipse[2] = tmp;
        
        if (sequence != null)
        {
            // convert to real units
            ellipse[0] *= sequence.getPixelSizeX();
            ellipse[1] *= sequence.getPixelSizeY();
            ellipse[2] *= sequence.getPixelSizeZ();
        }
        
        return ellipse;
    }
    
    /**
     * Compute the best fitting ellipsoid for the given component.<br>
     * This method is adapted from Yury Petrov's Matlab code and ported to Java by the BoneJ project
     * 
     * @param cc
     *            the component to fit
     * @param center
     *            (set to null if not wanted) the calculated ellipsoid center
     * @param radii
     *            (set to null if not wanted) the calculated ellipsoid radius in each
     *            eigen-direction
     * @param eigenVectors
     *            (set to null if not wanted) the calculated ellipsoid eigen-vectors
     * @param equation
     *            (set to null if not wanted) an array of size 9 containing the calculated ellipsoid
     *            equation
     * @throws IllegalArgumentException
     *             if the number of points in the component is too low (minimum is 9)
     * @throws SingularMatrixException
     *             if the component is flat (i.e. lies in a 2D plane)
     */
    private static void fitEllipse(ROI3D cc, Point3d center, Point3d radii, Vector3d[] eigenVectors, double[] equation) throws IllegalArgumentException
    {
        Point3D.Integer[] points = cc.getBooleanMask(true).getContourPoints();
        
        if (points.length < 9)
        {
            // System.err.println("Warning while fitting a 3D ellipsoid to ROI \"" + cc.getName() +
            // "\": the ROI is too small (at least 9 points are needed)");
            return;
        }
        
        double[][] d = new double[points.length][9];
        for (int i = 0; i < points.length; i++)
        {
            double x = points[i].x;
            double y = points[i].y;
            double z = points[i].z;
            d[i][0] = (x * x);
            d[i][1] = (y * y);
            d[i][2] = (z * z);
            d[i][3] = (2.0D * x * y);
            d[i][4] = (2.0D * x * z);
            d[i][5] = (2.0D * y * z);
            d[i][6] = (2.0D * x);
            d[i][7] = (2.0D * y);
            d[i][8] = (2.0D * z);
        }
        
        Matrix D = new Matrix(d);
        Matrix ones = ones(points.length, 1);
        
        Matrix V;
        try
        {
            V = D.transpose().times(D).inverse().times(D.transpose().times(ones));
            
            double[] v = V.getColumnPackedCopy();
            
            double[][] a = { { v[0], v[3], v[4], v[6] }, { v[3], v[1], v[5], v[7] }, { v[4], v[5], v[2], v[8] }, { v[6], v[7], v[8], -1.0D } };
            Matrix A = new Matrix(a);
            Matrix C = A.getMatrix(0, 2, 0, 2).times(-1.0D).inverse().times(V.getMatrix(6, 8, 0, 0));
            Matrix T = Matrix.identity(4, 4);
            T.setMatrix(3, 3, 0, 2, C.transpose());
            Matrix R = T.times(A.times(T.transpose()));
            double r33 = R.get(3, 3);
            Matrix R02 = R.getMatrix(0, 2, 0, 2);
            EigenvalueDecomposition E = new EigenvalueDecomposition(R02.times(-1.0D / r33));
            Matrix eVal = E.getD();
            Matrix eVec = E.getV();
            Matrix diagonal = diag(eVal);
            
            if (radii != null) radii.set(Math.sqrt(1.0D / diagonal.get(0, 0)), Math.sqrt(1.0D / diagonal.get(1, 0)), Math.sqrt(1.0D / diagonal.get(2, 0)));
            
            if (center != null) center.set(C.get(0, 0), C.get(1, 0), C.get(2, 0));
            
            if (eigenVectors != null && eigenVectors.length == 3)
            {
                eigenVectors[0] = new Vector3d(eVec.get(0, 0), eVec.get(0, 1), eVec.get(0, 2));
                eigenVectors[1] = new Vector3d(eVec.get(1, 0), eVec.get(1, 1), eVec.get(1, 2));
                eigenVectors[2] = new Vector3d(eVec.get(2, 0), eVec.get(2, 1), eVec.get(2, 2));
            }
            if (equation != null && equation.length == 9) System.arraycopy(v, 0, equation, 0, v.length);
        }
        catch (RuntimeException e)
        {
            // System.err.println("Warning while fitting a 3D ellipsoid to ROI \"" + cc.getName() +
            // "\": " + e.getMessage());
            return;
        }
    }
    
    /**
     * 2D direct ellipse fitting.<br>
     * (Java port of Chernov's MATLAB implementation of the direct ellipse fit)
     * 
     * @param roi
     *            the component to fit
     * @param center
     *            (set to null if not wanted) the calculated ellipse center
     * @param radii
     *            (set to null if not wanted) the calculated ellipse radius in each eigen-direction
     * @param angle
     *            (set to null if not wanted) the calculated ellipse orientation
     * @param equation
     *            (set to null if not wanted) a 6-element array, {a b c d f g}, which are the
     *            calculated algebraic parameters of the fitting ellipse: <i>ax</i><sup>2</sup> + 2
     *            <i>bxy</i> + <i>cy</i><sup>2</sup> +2<i>dx</i> + 2<i>fy</i> + <i>g</i> = 0. The
     *            vector <b>A</b> represented in the array is normed, so that ||<b>A</b>||=1.
     * @throws RuntimeException
     *             if the ellipse calculation fails (e.g. if a singular matrix is detected)
     */
    private static void fitEllipse(ROI2D roi, Point2d center, Point2d radii, VarDouble angle, Vector2d[] eigenVectors, double[] equation) throws RuntimeException
    {
        Point[] points = roi.getBooleanMask(true).getContourPoints();
        
        if (points.length < 4) return;
        
        Point2D ccenter = ROIMassCenterDescriptorsPlugin.computeMassCenter(roi).toPoint2D();
        
        double cx = ccenter.getX();
        double cy = ccenter.getY();
        
        double[][] d1 = new double[points.length][3];
        double[][] d2 = new double[points.length][3];
        
        for (int i = 0; i < d1.length; i++)
        {
            final double xixC = points[i].x - cx;
            final double yiyC = points[i].y - cy;
            
            d1[i][0] = xixC * xixC;
            d1[i][1] = xixC * yiyC;
            d1[i][2] = yiyC * yiyC;
            
            d2[i][0] = xixC;
            d2[i][1] = yiyC;
            d2[i][2] = 1;
        }
        
        Matrix D1 = new Matrix(d1);
        Matrix D2 = new Matrix(d2);
        
        Matrix S1 = D1.transpose().times(D1);
        
        Matrix S2 = D1.transpose().times(D2);
        
        Matrix S3 = D2.transpose().times(D2);
        
        Matrix T = (S3.inverse().times(-1)).times(S2.transpose());
        
        Matrix M = S1.plus(S2.times(T));
        
        double[][] m = M.getArray();
        double[][] n = { { m[2][0] / 2, m[2][1] / 2, m[2][2] / 2 }, { -m[1][0], -m[1][1], -m[1][2] }, { m[0][0] / 2, m[0][1] / 2, m[0][2] / 2 } };
        
        Matrix N = new Matrix(n);
        
        EigenvalueDecomposition E = N.eig();
        Matrix eVec = E.getV();
        
        Matrix R1 = eVec.getMatrix(0, 0, 0, 2);
        Matrix R2 = eVec.getMatrix(1, 1, 0, 2);
        Matrix R3 = eVec.getMatrix(2, 2, 0, 2);
        
        Matrix cond = (R1.times(4)).arrayTimes(R3).minus(R2.arrayTimes(R2));
        
        int _f = 0;
        for (int i = 0; i < 3; i++)
        {
            if (cond.get(0, i) > 0)
            {
                _f = i;
                break;
            }
        }
        Matrix A1 = eVec.getMatrix(0, 2, _f, _f);
        
        Matrix A = new Matrix(6, 1);
        A.setMatrix(0, 2, 0, 0, A1);
        A.setMatrix(3, 5, 0, 0, T.times(A1));
        
        double[] ell = A.getColumnPackedCopy();
        double a4 = ell[3] - 2 * ell[0] * cx - ell[1] * cy;
        double a5 = ell[4] - 2 * ell[2] * cy - ell[1] * cx;
        double a6 = ell[5] + ell[0] * cx * cx + ell[2] * cy * cy + ell[1] * cx * cy - ell[3] * cx - ell[4] * cy;
        A.set(3, 0, a4);
        A.set(4, 0, a5);
        A.set(5, 0, a6);
        A = A.times(1 / A.normF());
        
        ell = A.getColumnPackedCopy();
        
        if (equation != null && equation.length != 6) System.arraycopy(ell, 0, equation, 0, 6);
        
        // Convert the general ellipse equation ax2 + bxy + cy2 +dx + fy + g = 0
        // into geometric parameters: center, radii and orientation.
        final double a = ell[0];
        final double b = ell[1] / 2;
        final double c = ell[2];
        final double d = ell[3] / 2;
        final double f = ell[4] / 2;
        final double g = ell[5];
        
        // centre
        final double cX = (c * d - b * f) / (b * b - a * c);
        final double cY = (a * f - b * d) / (b * b - a * c);
        
        // semiaxis length
        final double af = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g);
        
        final double aL = Math.sqrt((af) / ((b * b - a * c) * (Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))));
        
        final double bL = Math.sqrt((af) / ((b * b - a * c) * (-Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))));
        double phi = 0;
        if (b == 0)
        {
            if (a <= c) phi = 0;
            else if (a > c) phi = Math.PI / 2;
        }
        else
        {
            if (a < c) phi = Math.atan(2 * b / (a - c)) / 2;
            else if (a > c) phi = Math.atan(2 * b / (a - c)) / 2 + Math.PI / 2;
        }
        
        if (center != null) center.set(cX, cY);
        if (radii != null) radii.set(aL, bL);
        if (angle != null) angle.setValue(phi);
        if (eigenVectors != null)
        {
            eigenVectors[0] = new Vector2d(Math.cos(phi), Math.sin(phi));
            eigenVectors[1] = new Vector2d(Math.cos(phi + Math.PI / 2), Math.sin(phi + Math.PI / 2));
        }
    }
    
    private static Matrix diag(Matrix matrix)
    {
        int min = Math.min(matrix.getRowDimension(), matrix.getColumnDimension());
        double[][] diag = new double[min][1];
        for (int i = 0; i < min; i++)
        {
            diag[i][0] = matrix.get(i, i);
        }
        return new Matrix(diag);
    }
    
    private static Matrix ones(int m, int n)
    {
        double[][] array = new double[m][n];
        for (double[] row : array)
            Arrays.fill(row, 1.0);
        return new Matrix(array, m, n);
    }
    
    @Override
    public String getMainPluginClassName()
    {
        return ROIMeasures.class.getName();
    }
}
