package plugins.adufour.roi;

import icy.roi.ROI2D;
import icy.sequence.Sequence;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.kernel.roi.roi3d.ROI3DStack;

public class ExtrudeROI extends EzPlug implements ROIBlock
{
    EzVarSequence inSequence = new EzVarSequence("Input sequence");
    
    EzVarBoolean  t          = new EzVarBoolean("time", true);
    
    EzVarBoolean  z          = new EzVarBoolean("depth", true);
    
    EzVarBoolean  c          = new EzVarBoolean("channels", true);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", inSequence.getVariable());
        inputMap.add("extrude along C", c.getVariable());
        inputMap.add("extrude along Z", z.getVariable());
        inputMap.add("extrude along T", t.getVariable());
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
    }
    
    @Override
    public void clean()
    {
    }
    
    @Override
    protected void execute()
    {
        extrudeROI(inSequence.getValue(true), c.getValue(), z.getValue(), t.getValue());
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(inSequence);
        addEzComponent(c);
        addEzComponent(z);
        addEzComponent(t);
    }
    
    public void extrudeROI(Sequence sequence, boolean alongC, boolean alongZ, boolean alongT)
    {
        for (ROI2D r : sequence.getROI2Ds())
        {
            @SuppressWarnings("unchecked")
            Class<ROI2D> typeOfROI2D = (Class<ROI2D>) r.getClass();
            
            Rectangle5D r5 = r.getBounds5D();
            
            if (alongC && r5.isInfiniteC() && sequence.getSizeC() > 1)
            {
                sequence.removeROI(r);
                for (int c = 0; c < sequence.getSizeC(); c++)
                {
                    ROI2D r2 = (ROI2D) r.getCopy();
                    r2.setC(c);
                    sequence.addROI(r2);
                }
            }
            
            if (alongZ && r5.isInfiniteZ() && sequence.getSizeZ() > 1)
            {                
                ROI3DStack<ROI2D> r3 = new ROI3DStack<ROI2D>(typeOfROI2D);
                
                for (int z = 0; z < sequence.getSizeZ(); z++)
                    r3.setSlice(z, (ROI2D) r.getCopy());
                
                sequence.removeROI(r);
                sequence.addROI(r3);
            }
            
            if (alongT && r5.isInfiniteT() && sequence.getSizeT() > 1)
            {
                sequence.removeROI(r);
                for (int t = 0; t < sequence.getSizeT(); t++)
                {
                    ROI2D r2 = (ROI2D) r.getCopy();
                    r2.setT(t);
                    sequence.addROI(r2);
                }
            }
        }
    }
}
