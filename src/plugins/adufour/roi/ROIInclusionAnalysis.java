package plugins.adufour.roi;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.apache.poi.ss.usermodel.Workbook;

import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.type.point.Point5D;
import icy.util.StringUtil;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;

/**
 * This plug-in takes 2 sets of ROI, and calculates spatial inclusion and distances from one set
 * w.r.t. to the other
 * 
 * @author Alexandre Dufour
 */
public class ROIInclusionAnalysis extends EzPlug implements ROIBlock
{
    // EzGUI
    
    EzVarSequence seqOuterROI    = new EzVarSequence("Take outer ROI from");
    EzVarSequence seqInnerROI    = new EzVarSequence("Take inner ROI from");
    EzVarBoolean  includeOverlap = new EzVarBoolean("Include overlapping ROI", true);
    
    // Headless
    
    VarROIArray   outerROI       = new VarROIArray("Outer (enclosing) ROI");
    VarROIArray   innerROI       = new VarROIArray("Inner (enclosed) ROI");
    
    // Output
    
    VarWorkbook   workbook       = new VarWorkbook("Analysis", (Workbook) null);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Outer ROI", outerROI);
        inputMap.add("Inner ROI", innerROI);
        inputMap.add("Include overlap", includeOverlap.getVariable());
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("workbook", workbook);
    }
    
    @Override
    public void clean()
    {
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(seqOuterROI);
        addEzComponent(seqInnerROI);
        addEzComponent(includeOverlap);
        getUI().setParametersIOVisible(false);
    }
    
    @Override
    public void execute()
    {
        if (!isHeadLess())
        {
            // EzGUI: take ROI from the specified sequences
            outerROI.setValue(seqOuterROI.getValue(true).getROIs().toArray(new ROI[0]));
            innerROI.setValue(seqInnerROI.getValue(true).getROIs().toArray(new ROI[0]));
        }
        
        Workbook wb = Workbooks.createEmptyWorkbook();
        int row = 0, summaryRow = 0;
        
        // Create the sheets
        IcySpreadSheet summarySheet = Workbooks.getSheet(wb, "Summary");
        IcySpreadSheet detailSheet = Workbooks.getSheet(wb, "Details");
        
        // Write headers
        detailSheet.setRow(0, "Enclosing ROI", "X", "Y", "Z", "Enclosed ROI", "X", "Y", "Z", "Raw dist. to edge", "Relative dist. to edge");
        summarySheet.setRow(0, "Enclosing ROI", "X", "Y", "Z", "nb. enclosed ROI", "Avg. dist to edge", "Avg. relative dist. to edge");
        
        for (ROI enclosing : outerROI)
        {
            summaryRow++;
            getStatus().setCompletion((double) summaryRow / outerROI.size());
            
            Point5D cOut = ROIMassCenterDescriptorsPlugin.computeMassCenter(enclosing);
            
            int nbInside = 0;
            double avgRawDist = 0, avgRelDist = 0;
            for (ROI enclosed : innerROI)
            {
                double[] distances = inclusionAnalysis(enclosing, enclosed, true);
                if (!Double.isNaN(distances[0]))
                {
                    Point5D cIn = ROIMassCenterDescriptorsPlugin.computeMassCenter(enclosed);
                    
                    row++;
                    nbInside++;
                    avgRawDist += distances[0];
                    avgRelDist += distances[1];
                    detailSheet.setRow(row, enclosing.getName(), (int) cOut.getX(), (int) cOut.getY(), (int) cOut.getZ(), enclosed.getName(), (int) cIn.getX(), (int) cIn.getY(),
                            (int) cIn.getZ(), StringUtil.toString(distances[0], 2), StringUtil.toString(distances[1], 2));
                }
            }
            avgRawDist /= nbInside;
            avgRelDist /= nbInside;
            summarySheet.setRow(summaryRow, enclosing.getName(), (int) cOut.getX(), (int) cOut.getY(), (int) cOut.getZ(), nbInside, StringUtil.toString(avgRawDist, 2),
                    StringUtil.toString(avgRelDist, 2));
        }
        
        // // Gather all relationships from each ROI in set A to all other ROI in set B
        //
        // HashMap<ROI, ArrayList<Relationship>> c2c = new HashMap<ROI, ArrayList<Relationship>>();
        // HashMap<ROI, ArrayList<Relationship>> c2e = new HashMap<ROI, ArrayList<Relationship>>();
        // HashMap<ROI, ArrayList<Relationship>> e2c = new HashMap<ROI, ArrayList<Relationship>>();
        // HashMap<ROI, ArrayList<Relationship>> e2e = new HashMap<ROI, ArrayList<Relationship>>();
        //
        // HashSet<ROI> insiders = new HashSet<ROI>();
        //
        // // Look for inclusions and intersections
        //
        // HashMap<ROI, Integer> nbInside = new HashMap<ROI, Integer>();
        // HashMap<ROI, Integer> nbOnEdge = new HashMap<ROI, Integer>();
        //
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // ROI roiACopy = roiA.getCopy();
        // ((ROI2D) roiA).setC(-1);
        //
        // nbInside.put(roiA, 0);
        // nbOnEdge.put(roiA, 0);
        //
        // c2c.put(roiA, new ArrayList<Relationship>());
        // c2e.put(roiA, new ArrayList<Relationship>());
        // e2c.put(roiA, new ArrayList<Relationship>());
        // e2e.put(roiA, new ArrayList<Relationship>());
        //
        // for (ROI roiB : enclosedROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // // in case the 2 ROI sets are the same, skip the obvious case
        // if (roiA == roiB) continue;
        //
        // ((ROI2D) roiB).setC(-1);
        //
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // boolean inside = roiA.contains(roiB);
        // boolean onEdge = !inside && roiA.intersects(roiB);
        //
        // if (inside || onEdge)
        // {
        // c2c.get(roiA).add(new Relationship(roiA, roiB, inside, onEdge, c2c(roiA, roiB)));
        // c2e.get(roiA).add(new Relationship(roiA, roiB, inside, onEdge, c2e(roiA, roiB)));
        // e2c.get(roiA).add(new Relationship(roiA, roiB, inside, onEdge, c2e(roiB, roiA)));
        // e2e.get(roiA).add(new Relationship(roiA, roiB, inside, onEdge, onEdge ? 0 : e2e(roiA,
        // roiB)));
        //
        // insiders.add(roiB);
        //
        // // increase counters
        // if (inside)
        // {
        // nbInside.put(roiA, nbInside.get(roiA) + 1);
        // }
        // else
        // {
        // nbOnEdge.put(roiA, nbOnEdge.get(roiA) + 1);
        // }
        // }
        // }
        // }
        //
        // // Handle the outsiders
        // for (ROI roiB : enclosedROI.getValue())
        // {
        // if (insiders.contains(roiB)) continue;
        //
        // double c2c_minDist = Double.MAX_VALUE;
        // ROI c2c_source = null;
        // double c2e_minDist = Double.MAX_VALUE;
        // ROI c2e_source = null;
        // double e2c_minDist = Double.MAX_VALUE;
        // ROI e2c_source = null;
        // double e2e_minDist = Double.MAX_VALUE;
        // ROI e2e_source = null;
        //
        // // find the closest ROI in set A
        //
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // if (roiA == roiB) continue;
        //
        // double c2c_dist = c2c(roiA, roiB);
        // if (c2c_dist < c2c_minDist)
        // {
        // c2c_minDist = c2c_dist;
        // c2c_source = roiA;
        // }
        //
        // double c2e_dist = c2e(roiA, roiB);
        // if (c2e_dist < c2e_minDist)
        // {
        // c2e_minDist = c2e_dist;
        // c2e_source = roiA;
        // }
        //
        // double e2c_dist = c2e(roiB, roiA);
        // if (e2c_dist < e2c_minDist)
        // {
        // e2c_minDist = e2c_dist;
        // e2c_source = roiA;
        // }
        //
        // double e2e_dist = e2e(roiA, roiB);
        // if (e2e_dist < e2e_minDist)
        // {
        // e2e_minDist = e2e_dist;
        // e2e_source = roiA;
        // }
        // }
        //
        // c2c.get(c2c_source).add(new Relationship(c2c_source, roiB, false, false, c2c_minDist));
        // c2e.get(c2e_source).add(new Relationship(c2e_source, roiB, false, false, c2e_minDist));
        // e2c.get(e2c_source).add(new Relationship(e2c_source, roiB, false, false, e2c_minDist));
        // e2e.get(e2e_source).add(new Relationship(e2e_source, roiB, false, false, e2e_minDist));
        // }
        //
        // // Now store the results in a nice workbook
        //
        // String a = "Enclosing ROI";
        // String b = "Enclosed ROI";
        //
        // Workbook wb = Workbooks.createEmptyWorkbook();
        // int row, summaryRow;
        //
        // // Create the sheets
        //
        // IcySpreadSheet summarySheet = Workbooks.getSheet(wb, "Summary");
        // IcySpreadSheet c2cSheet = Workbooks.getSheet(wb, "Centre to Centre");
        // IcySpreadSheet c2eSheet = Workbooks.getSheet(wb, "Centre to Edge");
        // IcySpreadSheet e2cSheet = Workbooks.getSheet(wb, "Edge to Centre");
        // IcySpreadSheet e2eSheet = Workbooks.getSheet(wb, "Edge to Edge");
        //
        // c2cSheet.setRow(0, a, b, "Inside", "On edge", "Outside", "Distance");
        // c2eSheet.setRow(0, a, b, "Inside", "On edge", "Outside", "Distance");
        // e2cSheet.setRow(0, a, b, "Inside", "On edge", "Outside", "Distance");
        // e2eSheet.setRow(0, a, b, "Inside", "On edge", "Outside", "Distance");
        //
        // summarySheet.setRow(0, a, "Contained " + b, "Intersecting " + b, "C2C (n)", "C2C (avg)",
        // "C2C (std)", "C2E (n)", "C2E (avg)", "C2E (std)", "E2C (n)", "E2C (avg)",
        // "E2C (std)", "E2E (n)", "E2E (avg)", "E2E (std)");
        //
        // summaryRow = 1;
        //
        // for (ROI roiA : enclosingROI.getValue())
        // summarySheet.setRow(summaryRow++, roiA.getName(), nbInside.get(roiA),
        // nbOnEdge.get(roiA));
        //
        // row = 1;
        // summaryRow = 1;
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // ArrayList<Relationship> rr = c2c.get(roiA);
        // if (rr.isEmpty())
        // {
        // summaryRow++;
        // continue;
        // }
        //
        // int d = 0;
        // double[] dists = new double[rr.size()];
        // for (Relationship r : rr)
        // {
        // dists[d++] = r.distance;
        // c2cSheet.setRow(row++, roiA.getName(), r.enclosed.getName(), r.inside, r.onEdge,
        // !r.inside && !r.onEdge, r.distance);
        // }
        //
        // // Summarize
        // summarySheet.setValue(summaryRow, 3, dists.length);
        // summarySheet.setValue(summaryRow, 4, ArrayMath.mean(dists));
        // summarySheet.setValue(summaryRow, 5, ArrayMath.std(dists, false));
        // summaryRow++;
        // }
        //
        // row = 1;
        // summaryRow = 1;
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // ArrayList<Relationship> rr = c2e.get(roiA);
        // if (rr.isEmpty())
        // {
        // summaryRow++;
        // continue;
        // }
        //
        // int d = 0;
        // double[] dists = new double[rr.size()];
        // for (Relationship r : rr)
        // {
        // dists[d++] = r.distance;
        // c2eSheet.setRow(row++, roiA.getName(), r.enclosed.getName(), r.inside, r.onEdge,
        // !r.inside && !r.onEdge, r.distance);
        // }
        //
        // // Summarize
        // summarySheet.setValue(summaryRow, 6, dists.length);
        // summarySheet.setValue(summaryRow, 7, ArrayMath.mean(dists));
        // summarySheet.setValue(summaryRow, 8, ArrayMath.std(dists, false));
        // summaryRow++;
        // }
        //
        // row = 1;
        // summaryRow = 1;
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // ArrayList<Relationship> rr = e2c.get(roiA);
        // if (rr.isEmpty())
        // {
        // summaryRow++;
        // continue;
        // }
        //
        // int d = 0;
        // double[] dists = new double[rr.size()];
        // for (Relationship r : rr)
        // {
        // dists[d++] = r.distance;
        // e2cSheet.setRow(row++, roiA.getName(), r.enclosed.getName(), r.inside, r.onEdge,
        // !r.inside && !r.onEdge, r.distance);
        // }
        //
        // // Summarize
        // summarySheet.setValue(summaryRow, 9, dists.length);
        // summarySheet.setValue(summaryRow, 10, ArrayMath.mean(dists));
        // summarySheet.setValue(summaryRow, 11, ArrayMath.std(dists, false));
        // summaryRow++;
        // }
        //
        // row = 1;
        // summaryRow = 1;
        // for (ROI roiA : enclosingROI.getValue())
        // {
        // if (Thread.currentThread().isInterrupted()) return;
        //
        // ArrayList<Relationship> rr = e2e.get(roiA);
        // if (rr.isEmpty())
        // {
        // summaryRow++;
        // continue;
        // }
        //
        // int d = 0;
        // double[] dists = new double[rr.size()];
        // for (Relationship r : rr)
        // {
        // dists[d++] = r.distance;
        // e2eSheet.setRow(row++, roiA.getName(), r.enclosed.getName(), r.inside, r.onEdge,
        // !r.inside && !r.onEdge, r.distance);
        // }
        //
        // // Summarize
        // summarySheet.setValue(summaryRow, 12, dists.length);
        // summarySheet.setValue(summaryRow, 13, ArrayMath.mean(dists));
        // summarySheet.setValue(summaryRow, 14, ArrayMath.std(dists, false));
        // summaryRow++;
        // }
        
        workbook.setValue(wb);
        
        if (!isHeadLess())
        {
            // EzGUI: display the workbook
            Workbooks.show(wb, "Inclusion analysis");
        }
    }
    
    /**
     * This method performs the inclusion analysis of a so-called "inner" ROI with respect to a
     * so-called "outer" ROI. It is first determined whether the specified "inner" ROI is contained
     * in (or overlaps with) the specified "outer" ROI. If so, two distance values are measured:
     * <ul>
     * <li>The raw distance (in pixels) from the center of the "inner" ROI to the edge of the
     * "outer" ROI</li>
     * <li>The relative distance (from 0 to 1) indicating where the "inner" ROI lies between the
     * center and the edge of the "outer" ROI.</li>
     * </ul>
     * Both distances are measured along an imaginary segment going from the center to the edge of
     * the "outer" ROI, and passing through the center of the "inner" ROI.
     * 
     * @param outerROI
     *            the outer (i.e. enclosing) ROI
     * @param innerROI
     *            the inner (i.e. enclosed) ROI
     * @param allowPartialOverlap
     *            set to <code>true</code> if partially overlapping ROI should be considered as
     *            "inside", or <code>false</code> if they should be discarded
     * @return A 2-element array containing the measured distances. The first element is the raw
     *         distance (in pixels) from the center of the "inner" ROI to the edge of the "outer"
     *         ROI (or <code>NaN</code> if the "inner" ROI is not related to the "outer" ROI). The
     *         second element is the relative distance from center to edge, i.e.:
     *         <ul>
     *         <li>0 if the enclosed ROI is at the center of the enclosing ROI</li>
     *         <li>1 if the enclosed ROI is at the edge of the enclosing ROI</li>
     *         <li>NaN if the enclosed ROI is not inside the enclosing ROI</li>
     *         </ul>
     */
    public static double[] inclusionAnalysis(ROI outerROI, ROI innerROI, boolean allowPartialOverlap)
    {
        boolean enclosure = allowPartialOverlap ? outerROI.intersects(innerROI) : outerROI.contains(innerROI);
        
        if (!enclosure) return new double[] { Double.NaN, Double.NaN };
        
        Point5D p5;
        
        // get the center of the enclosing ROI
        p5 = ROIMassCenterDescriptorsPlugin.computeMassCenter(outerROI);
        Point3d enclosingCenter = new Point3d(p5.getX(), p5.getY(), p5.getZ());
        
        // get the center of the enclosed ROI
        p5 = ROIMassCenterDescriptorsPlugin.computeMassCenter(innerROI);
        Point3d enclosedCenter = new Point3d(p5.getX(), p5.getY(), p5.getZ());
        
        // compute the (raw) inclusion distance (i.e. center to center)
        double distToCenter = enclosingCenter.distance(enclosedCenter);
        
        // compute (locally) the radius of the enclosing ROI
        // do this lazily by stepping away from the center
        double step = 0.1;
        Point3d p = new Point3d(enclosedCenter);
        Vector3d ray = new Vector3d();
        ray.sub(enclosedCenter, enclosingCenter);
        ray.normalize();
        ray.scale(step);
        
        // step away until we exit the ROI
        // NB: we might already be outside if the enclosed ROI is only overlapping
        if (outerROI instanceof ROI2D)
        {
            while (((ROI2D) outerROI).contains(p.x, p.y))
                p.add(ray);
        }
        else if (outerROI instanceof ROI3D)
        {
            while (((ROI3D) outerROI).contains(p.x, p.y, p.z))
                p.add(ray);
        }
        else
        {
            while (outerROI.contains(p.x, p.y, p.z, -1, -1))
                p.add(ray);
        }
        
        // the distance from p to the center is the "local" radius
        double localRadius = p.distance(enclosingCenter);
        double distToEdge = localRadius - distToCenter;
        return new double[] { distToEdge, distToCenter / localRadius };
    }
    
    // private static class Relationship
    // {
    // final ROI enclosing;
    // final ROI enclosed;
    // final boolean inside, onEdge;
    // final double distance;
    //
    // public Relationship(ROI source, ROI target, boolean inside, boolean onEdge, double distance)
    // {
    // this.enclosing = source;
    // this.enclosed = target;
    // this.inside = inside;
    // this.onEdge = onEdge;
    // this.distance = distance;
    // }
    // }
    //
    // /**
    // * @param roiA
    // * @param roiB
    // * @return center-to-center distance
    // */
    // static double c2c(ROI roiA, ROI roiB)
    // {
    // Rectangle5D boundsA = roiA.getBounds5D();
    // Point3d centerA = new Point3d(boundsA.getCenterX(), boundsA.getCenterY(),
    // boundsA.getCenterZ());
    //
    // Rectangle5D boundsB = roiB.getBounds5D();
    // Point3d centerB = new Point3d(boundsB.getCenterX(), boundsB.getCenterY(),
    // boundsB.getCenterZ());
    //
    // return centerA.distance(centerB);
    // }
    //
    // /**
    // * @param roiA
    // * @param roiB
    // * @return center-to-edge distance
    // */
    // static double c2e(ROI roiA, ROI roiB)
    // {
    // Rectangle5D boundsA = roiA.getBounds5D();
    // Point3d centerA = new Point3d(boundsA.getCenterX(), boundsA.getCenterY(),
    // boundsA.getCenterZ());
    //
    // double minDist = Double.MAX_VALUE;
    //
    // if (roiB instanceof ROI2D)
    // {
    // for (Point pt : ((ROI2D) roiB).getBooleanMask(true).getContourPoints())
    // {
    // double dist = centerA.distance(new Point3d(pt.x, pt.y, ((ROI2D) roiB).getZ()));
    // if (dist < minDist) minDist = dist;
    // }
    // return minDist;
    // }
    // else if (roiB instanceof ROI3D)
    // {
    // for (Point3D.Integer pt : ((ROI3D) roiB).getBooleanMask(true).getContourPoints())
    // {
    // double dist = centerA.distance(new Point3d(pt.x, pt.y, pt.z));
    // if (dist < minDist) minDist = dist;
    // }
    // return minDist;
    // }
    // else throw new UnsupportedOperationException("Cannot process a ROI of type " +
    // roiB.getClassName());
    // }
    //
    // /**
    // * @param roiA
    // * @param roiB
    // * @return edge-to-edge distance
    // */
    // static double e2e(ROI roiA, ROI roiB)
    // {
    // double minDist = Double.MAX_VALUE;
    //
    // if (roiA instanceof ROI2D)
    // {
    // for (Point ptA : ((ROI2D) roiA).getBooleanMask(true).getContourPoints())
    // {
    // if (roiB instanceof ROI2D)
    // {
    // for (Point ptB : ((ROI2D) roiB).getBooleanMask(true).getContourPoints())
    // {
    // double dist = ptA.distance(ptB);
    // if (dist < minDist) minDist = dist;
    // }
    // }
    // else if (roiB instanceof ROI3D)
    // {
    // Point3d ptA_3d = new Point3d(ptA.x, ptA.y, ((ROI2D) roiA).getZ());
    // for (Point3D.Integer ptB : ((ROI3D) roiB).getBooleanMask(true).getContourPoints())
    // {
    // double dist = ptA_3d.distance(new Point3d(ptB.x, ptB.y, ptB.z));
    // if (dist < minDist) minDist = dist;
    // }
    // }
    // }
    // return minDist;
    // }
    // else if (roiA instanceof ROI3D)
    // {
    // for (Point3D.Integer ptA : ((ROI3D) roiA).getBooleanMask(true).getContourPoints())
    // {
    // Point3d ptA_3d = new Point3d(ptA.x, ptA.y, ((ROI2D) roiA).getZ());
    //
    // for (Point3D.Integer ptB : ((ROI3D) roiB).getBooleanMask(true).getContourPoints())
    // {
    // double dist = ptA_3d.distance(new Point3d(ptB.x, ptB.y, ptB.z));
    // if (dist < minDist) minDist = dist;
    // }
    // }
    // return minDist;
    // }
    // else throw new UnsupportedOperationException("Cannot process a ROI of type " +
    // roiB.getClassName());
    // }
}
