package plugins.adufour.roi.mesh;

import javax.vecmath.Point3d;

import icy.image.colormap.IcyColorMap;
import icy.sequence.DimensionId;
import icy.sequence.Sequence;
import icy.vtk.VtkUtil;
import plugins.adufour.ezplug.EzLabel;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStatus;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarDimensionPicker;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import vtk.vtkDecimatePro;
import vtk.vtkImageConstantPad;
import vtk.vtkImageData;
import vtk.vtkMarchingCubes;
import vtk.vtkPolyData;
import vtk.vtkPolyDataConnectivityFilter;
import vtk.vtkSmoothPolyDataFilter;

public class IsoSurface extends EzPlug
{
    EzVarSequence sequence = new EzVarSequence("Input sequence");
    
    EzVarDimensionPicker frame = new EzVarDimensionPicker("Frame", DimensionId.T, sequence.getVariable(), true);
    
    EzVarDimensionPicker channel = new EzVarDimensionPicker("Channel", DimensionId.C, sequence.getVariable(), false);
    
    EzVarDouble isoValue = new EzVarDouble("Iso-value", 0, -1000000, 1000000, 1);
    
    EzVarBoolean keepLargest = new EzVarBoolean("Keep largest surface", false);
    
    EzVarInteger nbSmoothIterations = new EzVarInteger("Smoothing (iterations)", 0, 0, 1000, 1);
    
    @Override
    public void clean()
    {
    
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(new EzLabel("NB: rendering on the 2D viewer can be slow. It is recommended to switch to the 3D VTK viewer before using this plug-in."));
        addEzComponent(sequence);
        addEzComponent(frame);
        addEzComponent(channel);
        addEzComponent(isoValue);
        addEzComponent(keepLargest);
        addEzComponent(nbSmoothIterations);
    }
    
    @Override
    public void execute()
    {
        Sequence s = sequence.getValue(true);
        int tMin = frame.getValue();
        int tMax = frame.getValue();
        
        if (tMin == -1)
        {
            tMin = 0;
            tMax = s.getSizeT() - 1;
        }
        
        for (int t = tMin; t <= tMax; t++)
            s.addROI(buildIsoSurface(s, t, channel.getValue(), isoValue.getValue(), keepLargest.getValue(), nbSmoothIterations.getValue(), getStatus()));
    }
    
    /**
     * @param s
     *            the sequence to reconstruct
     * @param frame
     *            the frame to reconstruct (NB: cannot be -1)
     * @param channel
     *            the channel to reconstruct (NB: cannot be -1)
     * @param isoValue
     *            the intensity value to reconstruct
     * @return
     */
    public static ROI3DPolygonalMesh buildIsoSurface(Sequence s, int frame, int channel, double isoValue)
    {
        return buildIsoSurface(s, frame, channel, isoValue, false, 0, null);
    }
    
    /**
     * @param s
     *            the sequence to reconstruct
     * @param t
     *            the frame to reconstruct (NB: cannot be -1)
     * @param c
     *            the channel to reconstruct (NB: cannot be -1)
     * @param value
     *            the intensity value to reconstruct
     * @param keepLargest
     *            keeps the largest surface only (useful to remove small surfaces due to noise)
     * @param smoothIterations
     *            the number of smoothing iterations (or 0 if no smoothing is required)
     * @param status
     *            a status object to monitor the completion of this process (optional: set to
     *            <code>null</code> if not needed)
     * @return
     */
    public static ROI3DPolygonalMesh buildIsoSurface(Sequence s, int t, int c, double value, boolean keepLargest, int smoothIterations, EzStatus status)
    {
        vtkImageData imageData = VtkUtil.getImageData(s.getDataCopyXYZ(t, c), s.getDataType_(), s.getSizeX(), s.getSizeY(), s.getSizeZ(), 1);
        imageData.SetSpacing(s.getPixelSizeX(), s.getPixelSizeY(), s.getPixelSizeZ());
        
        // pad on all sides to guarantee close meshes
        vtkImageConstantPad pad = new vtkImageConstantPad();
        pad.SetInputData(imageData);
        int[] extent = imageData.GetExtent();
        extent[0]--; // min X
        extent[1]++; // max X
        extent[2]--; // min Y
        extent[3]++; // max Y
        extent[4]--; // min Z
        extent[5]++; // max Z
        pad.SetOutputWholeExtent(extent);
        
        status.setMessage("[T=" + t + "] Calculating the iso-surface...");
        vtkMarchingCubes mc = new vtkMarchingCubes();
        mc.SetInputConnection(pad.GetOutputPort());
        mc.SetValue(0, value);
        mc.Update();
        vtkPolyData meshData = mc.GetOutput();
        
        if (keepLargest)
        {
            status.setMessage("Extract the largest component");
            vtkPolyDataConnectivityFilter cc = new vtkPolyDataConnectivityFilter();
            cc.SetExtractionModeToLargestRegion();
            cc.SetInputData(meshData);
            cc.Update();
            meshData = cc.GetOutput();
        }
        
        vtkDecimatePro dec = new vtkDecimatePro();
        dec.SetInputData(meshData);
        dec.PreserveTopologyOn();
        dec.SetTargetReduction(0.9);
        dec.Update();
        meshData = dec.GetOutput();
        
        if (smoothIterations > 0)
        {
            status.setMessage("[T=" + t + "] Smoothing the mesh");
            vtkSmoothPolyDataFilter smoother = new vtkSmoothPolyDataFilter();
            smoother.SetRelaxationFactor(1);
            smoother.FeatureEdgeSmoothingOff();
            smoother.BoundarySmoothingOn();
            smoother.SetNumberOfIterations(smoothIterations);
            smoother.SetInputData(meshData);
            smoother.Update();
            
            meshData = smoother.GetOutput();
        }
        
        // make sure the mesh is not empty
        if (meshData.GetNumberOfPoints() == 0) return null;
        
        status.setMessage("[T=" + t + "] Building the ROI");
        Point3d pixelSize = new Point3d(s.getPixelSizeX(), s.getPixelSizeY(), s.getPixelSizeZ());
        ROI3DPolygonalMesh mesh = new ROI3DPolygonalMesh(pixelSize, meshData);
        mesh.setC(c);
        mesh.setT(t);
        mesh.setName("Isosurface: " + value);
        mesh.setColor(s.getColorMap(c).getColor(IcyColorMap.SIZE - 1));
        
        return mesh;
    }
}
