package plugins.adufour.roi.mesh.polyhedron;

import java.util.List;

import javax.vecmath.Point3d;

import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.adufour.roi.mesh.Vertex3D;

import vtk.vtkCell3D;
import vtk.vtkWedge;

/**
 * Specialized implementation of a polyhedron in the form of a wedge (2 parallel polygons with equal
 * number of points). <br/>
 * <br/>
 * Vertex ordering convention: (A1, ..., An, B1, ..., Bn)
 * <ul>
 * <li>Each half of the index buffer describes one face of the wedge</li>
 * <li>Vertex indices from different faces are pair-wise aligned</li>
 * <li>The front face is listed in counter-clockwise order</li>
 * </ul>
 * 
 * @author Alexandre Dufour
 */
public class Wedge3D extends Polyhedron3D
{
    public Wedge3D(int... vertexIndices)
    {
        super(vertexIndices);
        
        if (vertexIndices.length % 2 != 0) throw new IllegalArgumentException("A wedge is defined by an even number of vertices");
    }
    
    @Override
    public Wedge3D clone()
    {
        return new Wedge3D(vertexIndices);
    }
    
    @Override
    public boolean contains(Point3d point, ROI3DMesh<?> mesh)
    {
        return false;// FIXME
    }
    
    @Override
    public vtkCell3D createVTKCell()
    {
        return new vtkWedge();
    }
    
    @Override
    public double computeVolume(List<Vertex3D> vertices)
    {
        return Double.NaN;// FIXME
    }
}
