package plugins.adufour.roi.mesh.polyhedron;

import java.util.List;

import javax.vecmath.Point3d;

import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.adufour.roi.mesh.Vertex3D;

import vtk.vtkCell3D;
import vtk.vtkTetra;

/**
 * Specialized implementation of a polyhedron in the form of a tetrahedron. By convention, if vertex
 * indices (0,1,2) describe the base of the tetrahedron, then vertex index (3) is located in the
 * direction of the base's normal vector.
 * 
 * @author Alexandre Dufour
 */
public class Tetrahedron3D extends Polyhedron3D
{
    public Tetrahedron3D(int... vertexIndices)
    {
        super(vertexIndices);
        
        if (vertexIndices.length != 4) throw new IllegalArgumentException("A tetrahedron is defined by exactly 4 vertices");
    }
    
    @Override
    public Tetrahedron3D clone()
    {
        return new Tetrahedron3D(vertexIndices);
    }

    @Override
    public boolean contains(Point3d point, ROI3DMesh<?> mesh)
    {
        // FIXME
        return false;
    }
    
    @Override
    public vtkCell3D createVTKCell()
    {
        return new vtkTetra();
    }

    @Override
    public double computeVolume(List<Vertex3D> vertices)
    {
        // FIXME
        return Double.NaN;
    }
}
