package plugins.adufour.roi.mesh.polyhedron;

import icy.vtk.VtkUtil;

import java.io.File;

import javax.vecmath.Point3d;

import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import vtk.CellType;
import vtk.vtkCell;
import vtk.vtkCell3D;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkIdList;
import vtk.vtkMapper;
import vtk.vtkPointSet;
import vtk.vtkPoints;
import vtk.vtkUnstructuredGrid;
import vtk.vtkUnstructuredGridReader;
import vtk.vtkXMLUnstructuredGridReader;

/**
 * <h2>WARNING: Polyhedral meshes are still under development, do *not* use for production work!</h2>
 * <br/>
 * 3D <u>r</u>egion <u>o</u>f <u>i</u>nterest (ROI) defined as a polyhedral mesh (i.e. a connected
 * set of 3D polyhedrons defining the interior of the ROI).<br/>
 * <br/>
 * This data structure is the dual of {@link ROI3DPolygonalMesh polygon meshes} which only defines
 * the contour of the 3D ROI.
 * 
 * @author Alexandre Dufour
 */
public class ROI3DPolyhedralMesh extends ROI3DMesh<Polyhedron3D>
{
    private vtkUnstructuredGrid vtkGrid;
    
    /**
     * <h1>DO NOT USE! This constructor is for XML loading purposes only</h1>
     */
    public ROI3DPolyhedralMesh()
    {
        // throw new IcyHandledException("3D Polyhedron ROI are under development");
    }
    
    @Override
    protected vtkPointSet createVTKCells()
    {
        return vtkGrid = new vtkUnstructuredGrid();
    }
    
    @Override
    protected vtkMapper createVTKMapper()
    {
        return new vtkDataSetMapper();
    }
    
    /**
     * {@inheritDoc} <h3>Currently supported cell types:</h3>
     * <ul>
     * <li>{@link Tetrahedron3D}</li>
     * <li>{@link Wedge3D}</li>
     * <li>{@link Pyramid3D}</li>
     * </ul>
     */
    @Override
    public Polyhedron3D createCell(CellType type, int... vertexIndices)
    {
        if (type == CellType.POLYGON) throw new IllegalArgumentException("Cannot create a polygon for a polyhedral mesh");
        
        switch (type)
        {
        case PYRAMID:
            return new Pyramid3D(vertexIndices);
        case WEDGE:
            return new Wedge3D(vertexIndices);
        case TETRA:
            return new Tetrahedron3D(vertexIndices);
        default:
            throw new UnsupportedOperationException("Cannot insert a cell of type: " + type + " into a polyhedral ROI");
        }
    }
    
    @Override
    public boolean contains(double x, double y, double z)
    {
        return contains(new Point3d(x, y, z));
    }
    
    /**
     * Checks whether the specified point lies inside this mesh
     * 
     * @param point
     * @return
     */
    public boolean contains(Point3d point)
    {
        // check if any cell contains the point
        for (Polyhedron3D cell : cells)
            if (cell.contains(point, this)) return true;
        
        return false;
    }
    
    @Override
    protected void updateVTKCells(vtkPoints newPoints)
    {
        vtkGrid.SetPoints(newPoints);
        
        for (Polyhedron3D polyhedron : cells)
        {
            vtkCell3D cell = polyhedron.createVTKCell();
            
            vtkIdList ids = cell.GetPointIds();
            
            // assume the cell indices are in proper ordering
            for (int i = 0; i < polyhedron.size; i++)
                ids.SetId(i, polyhedron.vertexIndices[i]);
            
            vtkGrid.InsertNextCell(cell.GetCellType(), ids);
        }
    }
    
    @Override
    public double getNumberOfContourPoints()
    {
        double surface = Double.NaN;
        
        // TODO find the outer surface of this polyhedral mesh, and calculate its surface area
        
        return surface;
    }
    
    @Override
    public double getNumberOfPoints()
    {
        double volume = 0;
        
        for (Polyhedron3D polyhedron : cells)
            volume += polyhedron.computeVolume(vertices);
        
        return volume;
    }
    
    @Override
    public boolean loadFromVTK(File vtkFile, boolean useLegacyReader)
    {
        vtkUnstructuredGrid gridData;
        
        if (useLegacyReader)
        {
            vtkUnstructuredGridReader legacyReader = new vtkUnstructuredGridReader();
            legacyReader.SetFileName(vtkFile.getPath());
            legacyReader.Update();
            legacyReader.GetOutput();
            gridData = legacyReader.GetOutput();
            legacyReader.Delete();
        }
        else
        {
            vtkXMLUnstructuredGridReader reader = new vtkXMLUnstructuredGridReader();
            reader.SetFileName(vtkFile.getPath());
            reader.Update();
            gridData = reader.GetOutput();
            reader.Delete();
        }
        
        return load(gridData);
    }
    
    private boolean load(vtkUnstructuredGrid gridData)
    {
        // load points (generically)
        setVertexData(gridData.GetPoints());
        
        // load cells
        vtkCellArray gridCells = gridData.GetCells();
        
        int[] indexBuffer = VtkUtil.getArray(gridCells.GetData());
        
        int nCells = gridCells.GetNumberOfCells();
        for (int cellID = 0, idx = 0; cellID < nCells; cellID++)
        {
            vtkCell cell = gridData.GetCell(cellID);
            
            int nPointsPerFace = indexBuffer[idx++];
            int[] vertexIndices = new int[nPointsPerFace];
            System.arraycopy(indexBuffer, idx, vertexIndices, 0, nPointsPerFace);
            addCell(createCell(CellType.GetCellType(cell.GetCellType()), vertexIndices));
            idx += nPointsPerFace;
        }
        
        return true;
    }
}
