package plugins.adufour.roi.mesh.polyhedron;

import java.util.List;

import javax.vecmath.Point3d;

import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.adufour.roi.mesh.Vertex3D;

import vtk.vtkCell3D;
import vtk.vtkPyramid;

/**
 * Specialized implementation of a polyhedron in the form of a quad-based pyramid. By convention, if
 * vertex indices (0,1,2,3) describe the (coplanar) base of the pyramid, then vertex index (4) is
 * located in the direction of the base's normal vector.
 * 
 * @author Alexandre Dufour
 */
public class Pyramid3D extends Polyhedron3D
{
    public Pyramid3D(int... vertexIndices)
    {
        super(vertexIndices);
    }
    
    @Override
    public Pyramid3D clone()
    {
        return new Pyramid3D(vertexIndices);
    }
    
    @Override
    public boolean contains(Point3d point, ROI3DMesh<?> mesh)
    {
        return false;// FIXME
    }
    
    @Override
    public vtkCell3D createVTKCell()
    {
        return new vtkPyramid();
    }
    
    @Override
    public double computeVolume(List<Vertex3D> vertices)
    {
        return Double.NaN;// FIXME
    }
}
