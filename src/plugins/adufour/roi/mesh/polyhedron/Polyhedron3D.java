package plugins.adufour.roi.mesh.polyhedron;

import java.util.List;

import javax.vecmath.Point3d;

import plugins.adufour.roi.mesh.Cell3D;
import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.adufour.roi.mesh.Vertex3D;

import vtk.vtkCell3D;

/**
 * Generic structural element of a {@link ROI3DPolyhedralMesh 3D polyhedral mesh}
 * 
 * @see Tetrahedron3D
 * @author Alexandre Dufour
 */
public abstract class Polyhedron3D extends Cell3D
{
    /**
     * Creates a new polygon from the specified vertex indices. This constructor is protected as it
     * should not be used directly by client code. Client code should use
     * {@link ROI3DPolyhedralMesh#createCell(int[])} instead
     * 
     * @param vertexIndices
     *            the vertex indices, in an order allowed by the local convention
     */
    protected Polyhedron3D(int... vertexIndices)
    {
        super(vertexIndices);
    }
    
    /**
     * Checks whether this polyhedron contains the specified point, taking vertex coordinates from
     * the specified list
     * 
     * @param point
     * @param mesh
     *            the mesh where vertex coordinates for this polyhedron should be fetched
     * @return
     */
    public abstract boolean contains(Point3d point, ROI3DMesh<?> mesh);
    
    /**
     * @return a VTK representation of this polyhedron
     */
    public abstract vtkCell3D createVTKCell();
    
    /**
     * Computes the volume of this polyhedron, using the specified list to fetch vertex coordinates
     * 
     * @param vertices
     *            the vertex list to fetch from
     * @return the volume of this polyhedron
     */
    public abstract double computeVolume(List<Vertex3D> vertices);
}
