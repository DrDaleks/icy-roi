package plugins.adufour.roi.mesh;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import icy.canvas.IcyCanvas;
import icy.painter.VtkPainter;
import icy.roi.ROI3D;
import icy.sequence.Sequence;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.system.thread.ThreadUtil;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle3D;
import icy.util.XMLUtil;
import plugins.adufour.roi.mesh.polygon.Polygon3D;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import plugins.adufour.roi.mesh.polyhedron.ROI3DPolyhedralMesh;
import plugins.kernel.canvas.VtkCanvas;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi3d.ROI3DArea;
import vtk.CellType;
import vtk.vtkActor;
import vtk.vtkMapper;
import vtk.vtkOutlineFilter;
import vtk.vtkPointSet;
import vtk.vtkPoints;
import vtk.vtkPolyDataMapper;
import vtk.vtkProp;
import vtk.vtkUnsignedCharArray;
import vtk.vtkXMLDataSetWriter;

/**
 * Generic class defining a 3D region of interest using a discrete 3D geometry representation. This
 * library provides the two major types of mesh representation:
 * <ul>
 * <li>Polygonal meshes (defining the region via its surface, see {@link ROI3DPolygonalMesh})</li>
 * <li>Polyhederal meshes (defining the region by its interior, see {@link ROI3DPolyhedralMesh})
 * </li>
 * </ul>
 * The VTK library provided with Icy already contains all the necessary tools to manipulate and
 * visualize 3D data sets, yet its low-levelness can be complex for beginners, while potential bugs
 * caused in the C++ back-end may cause the entire application to crash instead of generating
 * elegant bug reports. The main benefits of this library are listed below: <br/>
 * <br/>
 * <ul>
 * <li>Intermediate Java API, much simpler to use than the VTK back-end</li>
 * <li>Native integration with Icy's Sequence / Viewer / ROI system</li>
 * <li>Eliminates most of the boiler-plate to simplify user code as much as possible</li>
 * </ul>
 * 
 * @author Alexandre Dufour
 */
public abstract class ROI3DMesh<C extends Cell3D> extends ROI3D
{
    /**
     * Keep track of a list of painters to clean stuff properly
     */
    private ArrayList<MeshPainter> painters;
    
    private class MeshPainter extends ROIPainter implements VtkPainter
    {
        protected final vtkActor mainActor = new vtkActor();
        protected final vtkActor outlineActor = new vtkActor();
        
        private boolean showBoundingBoxIn2D = false;
        
        public MeshPainter()
        {
            mainActor.PickableOn();
            if (!vtkInitialized) initVTK();
            
            // create a new mapper for the mesh data
            vtkMapper mapper = createVTKMapper();
            mapper.SetInputDataObject(vtkCells);
            mainActor.SetMapper(mapper);
            
            // outline (shown when the ROI is selected)
            vtkPolyDataMapper outlineMapper = new vtkPolyDataMapper();
            outlineMapper.SetInputConnection(vtkOutlineFilter.GetOutputPort());
            outlineActor.SetMapper(outlineMapper);
            outlineActor.PickableOff();
            outlineActor.VisibilityOff();
        }
        
        @Override
        public void setColor(Color color)
        {
            outlineActor.GetProperty().SetColor(color.getRed() / 255., color.getGreen() / 255., color.getBlue() / 255.);
            mainActor.GetProperty().SetColor(color.getRed() / 255., color.getGreen() / 255., color.getBlue() / 255.);
            super.setColor(color);
        }
        
        @Override
        public vtkProp[] getProps()
        {
            return new vtkProp[] { mainActor, outlineActor };
        }
        
        @Override
        public void mouseClick(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
        {
            if (e.isConsumed() || getT() >= 0 && canvas.getPositionT() != getT()) return;
            
            if (canvas instanceof VtkCanvas)
            {
                VtkCanvas vtk = (VtkCanvas) canvas;
                
                // pick
                vtkProp prop = vtk.pickProp(e.getX(), e.getY());
                
                if (prop == this.mainActor)
                {
                    setSelected(!isSelected());
                }
            }
            
            super.mouseClick(e, imagePoint, canvas);
        }
        
        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            int currentT = getT();
            
            if (g != null) // 2D viewer
            {
                // only paint detections on the current frame
                if (currentT >= 0 && currentT != canvas.getPositionT()) return;
                
                int posZ = canvas.getPositionZ();
                
                Rectangle3D bounds = getBounds3D();
                
                if (posZ >= bounds.getMinZ() && posZ <= bounds.getMaxZ())
                {
                    if (showBoundingBoxIn2D)
                    {
                        setStroke(3.0);
                        g.setStroke(new BasicStroke((float) getAdjustedStroke(canvas)));
                        g.setColor(getColor());
                        
                        g.draw(new Rectangle2D.Double(bounds.getMinX(), bounds.getMinY(), bounds.getSizeX(), bounds.getSizeY()));
                    }
                    else
                    {
                        getBooleanMask(true);
                        ROI2DArea slice = mask.getSlice(posZ);
                        if (slice != null)
                        {
                            ROIPainter maskPainter = mask.getSlice(posZ).getOverlay();
                            maskPainter.setColor(getColor());
                            maskPainter.paint(g, sequence, canvas);
                        }
                    }
                }
            }
            else if (canvas instanceof VtkCanvas)
            {
                // set the visibility
                boolean visible = (currentT == -1 || currentT == canvas.getPositionT());
                
                if (visible)
                {
                    mainActor.VisibilityOn();
                    mainActor.PickableOn();
                    if (isSelected())
                    {
                        outlineActor.VisibilityOn();
                    }
                    else
                    {
                        outlineActor.VisibilityOff();
                    }
                }
                else
                {
                    outlineActor.VisibilityOff();
                    mainActor.PickableOff();
                    mainActor.VisibilityOff();
                }
            }
            else
            {
                // other viewers
            }
        }
        
        @Override
        public void remove()
        {
            mainActor.VisibilityOff();
            outlineActor.VisibilityOff();
            super.remove();
        }
    }
    
    private boolean vtkInitialized;
    
    protected vtkUnsignedCharArray vtkColors;
    
    /**
     * <ul>
     * <li><code>true</code> mesh vertices are colored according to the {@link #updateVTKColor()}
     * method</li>
     * <li><code>false</code> mesh vertices are colored uniformly (at the VTK actor level) using the
     * ROI color</li>
     * </ul>
     */
    protected boolean vtkCustomColoring = false;
    
    protected vtkPointSet vtkCells;
    
    /**
     * Filter used to create the bounding box of the ROI (shown when the ROI is selected)
     */
    protected vtkOutlineFilter vtkOutlineFilter;
    
    /**
     * Creates the VTK structure that will contain the mesh data
     * 
     * @return the VTK data holding the mesh contents
     * @see ROI3DPolygonalMesh#createVTKCells()
     */
    protected abstract vtkPointSet createVTKCells();
    
    /**
     * @return a new VTK mapper that will be used to map the mesh data on screen
     */
    protected abstract vtkMapper createVTKMapper();
    
    protected void initVTK()
    {
        vtkInitialized = true;
        
        vtkColors = new vtkUnsignedCharArray();
        vtkColors.SetNumberOfComponents(3);
        
        vtkCells = createVTKCells();
        
        vtkOutlineFilter = new vtkOutlineFilter();
        vtkOutlineFilter.SetInputData(vtkCells);
    }
    
    /**
     * Updates the internal VTK structure for display purposes
     */
    protected void updateVTK()
    {
        // 1) Update the vertex buffer first
        
        int nVerts = vertices.size();
        
        // take the opportunity to update the mass center
        int nRealVerts = 0;
        Point3d center = new Point3d();
        
        vtkPoints vtkPoints = new vtkPoints();
        vtkPoints.SetNumberOfPoints(nVerts);
        
        for (int i = 0; i < nVerts; i++)
        {
            Vertex3D v = vertices.get(i);
            
            if (v == null)
            {
                vtkPoints.SetPoint(i, Double.NaN, Double.NaN, Double.NaN);
            }
            else
            {
                nRealVerts++;
                
                vtkPoints.SetPoint(i, v.position.x, v.position.y, v.position.z);
                
                center.add(v.position);
            }
        }
        
        massCenter.scale(1.0 / nRealVerts, center);
        
        // 2) Update the cell data
        updateVTKCells(vtkPoints);
        
        // 3) Update color information if needed
        if (vtkCustomColoring) updateVTKColor();
    }
    
    /**
     * This method performs custom coloring of the mesh vertices. It is used only if
     * {@link #vtkCustomColoring} is set to <code>true</code>.<br/>
     * NOTE: the default implementation will color the mesh uniformly using the ROI's color (i.e. it
     * is equivalent to setting {@link #vtkCustomColoring} to <code>false</code>) and therefore has
     * limited interest. However, this method can be overridden (and used as a sample code) to
     * provide custom vertex coloring along the mesh.
     * 
     * @param vtkCells
     */
    protected void updateVTKColor()
    {
        // Color color = getColor();
        // int r = color.getRed(), g = color.getGreen(), b = color.getBlue();
        //
        // int nVerts = vtkCells.GetNumberOfPoints();
        // vtkColors.SetNumberOfTuples(nVerts);
        //
        // for (int i = 0; i < nVerts; i++)
        // vtkColors.SetTuple3(i, r, g, b);
        //
        ThreadUtil.invoke(new Runnable()
        {
            @Override
            public void run()
            {
                vtkCells.GetPointData().SetScalars(vtkColors);
            }
        }, false);
    }
    
    /**
     * Converts the mesh cells to the local VTK structures for display
     */
    protected abstract void updateVTKCells(vtkPoints points);
    
    @Override
    public boolean saveToXML(Node node)
    {
        if (!super.saveToXML(node)) return false;
        
        try
        {
            XMLUtil.setAttributeDoubleValue((Element) node, "PixelSizeXY", pixelSize.x);
            XMLUtil.setAttributeDoubleValue((Element) node, "PixelSizeZ", pixelSize.z);
            
            // write to a temporary (VTK XML) file and re-read from it
            File file = File.createTempFile("mesh", ".vtk");
            file.deleteOnExit();
            
            saveToVTK(file);
            
            Document doc = XMLUtil.loadDocument(file);
            Node vtk = node.getOwnerDocument().importNode(doc.getDocumentElement(), true);
            node.appendChild(vtk);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public boolean saveToVTK(File vtkFile)
    {
        optimizeVertexBuffer();
        
        vtkXMLDataSetWriter wr = new vtkXMLDataSetWriter();
        wr.SetInputData(vtkCells);
        wr.SetFileName(vtkFile.getPath());
        wr.Update();
        wr.Delete();
        
        return true;
    }
    
    @Override
    public boolean loadFromXML(Node node)
    {
        if (!super.loadFromXML(node)) return false;
        
        try
        {
            Element e = (Element) node;
            pixelSize.x = pixelSize.y = XMLUtil.getAttributeDoubleValue(e, "PixelSizeXY", 1.0);
            pixelSize.z = XMLUtil.getAttributeDoubleValue(e, "PixelSizeZ", 1.0);
            
            // General strategy: store the data into a temp file, and let VTK read it
            File file = File.createTempFile("mesh", ".vtkfile");
            file.deleteOnExit();
            
            // legacy formats may store an ASCII representation of the file
            String vtkString = XMLUtil.getAttributeValue(e, "VTK", null);
            
            boolean useLegacyReader = (vtkString != null);
            
            if (useLegacyReader)
            {
                FileWriter writer = new FileWriter(file);
                writer.write(vtkString);
                writer.flush();
                writer.close();
            }
            else
            {
                // the format is probably XML
                Node vtkXML = XMLUtil.getChild(node, "VTKFile");
                if (vtkXML == null)
                {
                    return false;
                }
                
                Document doc = XMLUtil.createDocument(false);
                doc.appendChild(doc.importNode(XMLUtil.getChild(node, "VTKFile"), true));
                XMLUtil.saveDocument(doc, file);
            }
            
            return loadFromVTK(file, useLegacyReader);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
        return false;
    }
    
    /**
     * Loads the mesh data from the specified VTK file. This method will attempt to guess the format
     * of the specified file and call the appropriate loader, which will result in slightly slower
     * performance. If the format is known in advance, use the {@link #loadFromVTK(File, boolean)}
     * method instead.
     * 
     * @param vtkFile
     *            the VTK file to read from
     * @return <code>true</code> if the file successfully loaded, <code>false</code> otherwise
     */
    public boolean loadFromVTK(File vtkFile)
    {
        // How to test: try to load as XML, and switch to legacy if it fails
        Document vtkXML = XMLUtil.loadDocument(vtkFile);
        
        boolean success = loadFromVTK(vtkFile, vtkXML == null);
        
        if (success) roiChanged(true);
        
        return success;
    }
    
    /**
     * Loads the mesh data from the specified VTK file
     * 
     * @param vtkFile
     *            the VTK file to read from
     * @param useLegacyReader
     *            <ul>
     *            <li><code>true</code> if the file uses the old legacy (plain text or binary)
     *            format</li>
     *            <li><code>false</code> if the file uses the (recommended) XML format</li>
     *            </ul>
     * @return <code>true</code> if the file successfully loaded, <code>false</code> otherwise
     */
    public abstract boolean loadFromVTK(File vtkFile, boolean useLegacyReader);
    
    @Override
    public void roiChanged(boolean contentChanged)
    {
        if (contentChanged)
        {
            maskUpToDate = false;
            updateVTK();
        }
        
        super.roiChanged(contentChanged);
    }
    
    public void setColor(Color value)
    {
        vtkCustomColoring = false;
        super.setColor(value);
    }
    
    public void setColorPerVertex(Color[] colors)
    {
        vtkCustomColoring = true;
        
        if (vtkColors.GetNumberOfTuples() != colors.length) vtkColors.SetNumberOfTuples(colors.length);
        
        for (int i = 0; i < colors.length; i++)
        {
            Color color = colors[i];
            int r = color.getRed();
            int g = color.getGreen();
            int b = color.getBlue();
            vtkColors.SetTuple3(i, r, g, b);
        }
        
        roiChanged(true);
    }
    
    @Override
    protected ROIPainter createPainter()
    {
        if (painters == null) painters = new ArrayList<MeshPainter>();
        MeshPainter meshPainter = new MeshPainter();
        meshPainter.setName(getName());
        painters.add(meshPainter);
        return meshPainter;
    }
    
    @Override
    public void remove(boolean canUndo)
    {
        for (MeshPainter meshPainter : painters)
            meshPainter.remove();
        painters.clear();
        
        super.remove(canUndo);
    }
    
    /** Multi-thread processor used for various mesh operations */
    protected final static Processor processor = new Processor(SystemUtil.getNumberOfCPUs());
    
    /**
     * The list of cells forming this mesh. Note that any change brought to this list should be
     * followed by a call to {@link #roiChanged()} to update visualization
     */
    protected final List<C> cells = new ArrayList<C>();
    
    /**
     * The list of mesh vertices. Note that any change brought to this list should be followed by a
     * call to {@link #roiChanged()} to update visualization
     */
    protected ArrayList<Vertex3D> vertices = new ArrayList<Vertex3D>();
    
    protected final Point3d massCenter = new Point3d();
    
    /**
     * A mask representation of this ROI (useful for 2D display and intensity measurements)
     */
    protected final ROI3DArea mask = new ROI3DArea();
    
    protected boolean maskUpToDate = false;
    
    protected boolean maskUpdating = false;
    
    /**
     * The size of a pixel in real units. This is a fundamental information to convert from pixel to
     * real coordinate system
     */
    protected final Tuple3d pixelSize = new Point3d(1.0, 1.0, 1.0);
    
    /**
     * Creates a clone of the specified contour
     * 
     * @param contour
     */
    @SuppressWarnings("unchecked")
    @Override
    public ROI3DMesh<C> clone()
    {
        // clone vertices
        ArrayList<Vertex3D> newVertices = new ArrayList<Vertex3D>(vertices.size());
        for (Vertex3D v : vertices)
            newVertices.add(v == null ? null : v.clone());
        
        // clone cells
        ArrayList<C> newCells = new ArrayList<C>(cells.size());
        for (C cell : cells)
            newCells.add((C) cell.clone());
        
        try
        {
            // clone the mesh
            ROI3DMesh<C> clone = getClass().newInstance();
            clone.setPixelSize(pixelSize);
            clone.setVertexData(newVertices);
            clone.setCellData(newCells);
            
            clone.setColor(getColor());
            clone.setT(getT());
            clone.setC(getC());
            
            clone.roiChanged(true);
            
            return clone;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Clears and replaces the vertices of this mesh by the specified vertex array. This is a
     * low-level method for bulk assignment, therefore the ROI is not notified of any change to its
     * internal data (a call to {@link #roiChanged()} is required)<br/>
     * <br/>
     * <b>Note:</b> the vertices in the provided list are not copied, therefore any change to their
     * contents after calling this method will still affect this mesh
     * 
     * @param newVertices
     *            the new vertices of this mesh
     */
    protected void setVertexData(Collection<Vertex3D> newVertices)
    {
        synchronized (vertices)
        {
            vertices.clear();
            
            vertices.ensureCapacity(newVertices.size());
            
            vertices.addAll(newVertices);
        }
    }
    
    /**
     * Clears and replaces the vertices of this mesh by the specified VTK point array. This is a
     * low-level method for bulk assignment, therefore the ROI is not notified of any change to its
     * internal data (a call to {@link #roiChanged()} is required)
     * 
     * @param points
     *            the new vertices of this mesh, in the form of a VTK point array
     */
    protected void setVertexData(vtkPoints points)
    {
        this.vertices.clear();
        
        int nPoints = points.GetNumberOfPoints();
        
        vertices.ensureCapacity(nPoints);
        
        for (int i = 0; i < nPoints; i++)
        {
            double[] xyz = points.GetPoint(i);
            Point3d position = new Point3d(xyz);
            addVertex(createVertex(position), false, false);
        }
    }
    
    /**
     * Clears and replaces the cells of this mesh by the specified cell array. This is a low-level
     * method for bulk assignment, therefore the ROI is not notified of any change to its internal
     * data (a call to {@link #roiChanged()} is required)<br/>
     * <br/>
     * <b>Note:</b> the cells in the provided list are not copied, therefore any change to their
     * contents after calling this method will still affect this mesh
     * 
     * @param newCells
     *            the new cells of this mesh
     */
    protected void setCellData(Collection<C> newCells)
    {
        synchronized (this)
        {
            cells.clear();
            
            if (cells instanceof ArrayList)
            {
                ((ArrayList<C>) cells).ensureCapacity(newCells.size());
            }
            
            cells.addAll(newCells);
        }
    }
    
    /**
     * Creates and adds a new face to this mesh using the specified vertex indices within the mesh's
     * existing vertex buffer.
     * 
     * @param vertices
     *            the face vertices, in counter-clockwise ordering
     * @throws MeshTopologyException
     */
    public void addCell(C cell)
    {
        synchronized (this)
        {
            cells.add(cell);
        }
    }
    
    /**
     * Creates a new cell for this mesh (without adding it), using the specified vertex indices.
     * <br/>
     * <br/>
     * This method is internally used when calling {@link #addCell(int[])}, allowing overriding
     * classes to provide their own {@link Polygon3D} implementation.
     * 
     * @param type
     *            the type of cell to add
     * @param vertexIndices
     *            the indices of the vertices forming the mesh, in counter-clockwise order
     * @return the newly created face
     */
    protected abstract C createCell(CellType type, int... vertexIndices);
    
    public void replaceCell(C oldCell, C newCell)
    {
        synchronized (this)
        {
            cells.set(cells.indexOf(oldCell), newCell);
        }
    }
    
    /**
     * @param index
     *            the index of the cell to retrieve
     * @return the cell stored at the specified index
     */
    public C getCell(int index)
    {
        return cells.get(index);
    }
    
    /**
     * @return the number of cells in this mesh
     */
    public int getNumberOfCells()
    {
        return cells.size();
    }
    
    /**
     * @return a safe copy of the list of cells (changes to this list will not affect the mesh)
     */
    public List<C> getCells()
    {
        List<C> shallowCopy = new ArrayList<C>(cells.size());
        shallowCopy.addAll(cells);
        return shallowCopy;
    }
    
    /**
     * Adds the specified vertex to this mesh (if no vertex already exists at this vertex location)
     * 
     * @param v
     *            the vertex to add
     * @return the index of the new vertex, or that of the old vertex at that location (if any)
     */
    public int addVertex(Vertex3D v)
    {
        return addVertex(v, true, true);
    }
    
    /**
     * Adds a new vertex to this mesh and returns its index in the vertex array. Based on the
     * specified options, if a vertex already existed at that location, this method will return the
     * index of the existing vertex instead of adding a new one.
     * 
     * @param v
     *            the coordinates of the vertex to create and add<br/>
     *            <br/>
     * @param mergeDuplicates
     *            <ul>
     *            <li><code>true</code>: checks whether this mesh already contains a vertex at the
     *            specified position (and should return its index instead of adding the specified
     *            vertex)</li>
     *            <li><code>false</code>: adds the specified vertex to the list, whether or not
     *            another exists at the same location (this is recommended when loading VTK files
     *            which should be consistent by default)</li>
     *            </ul>
     * @param tidy
     *            if the vertex is added, this parameter indicates how the vertex should be stored:
     *            <br/>
     *            <ul>
     *            <li><code>true</code>: the vertex is stored in the first available
     *            <code>null</code> position in the array (or at the end if none exist)</li>
     *            <li><code>false</code>: the vertex is always stored at the end of the array (this
     *            is only recommended when loading existing mesh data to preserve vertex ordering)
     *            </li>
     *            </ul>
     * @return the index of the new vertex, or that of the old vertex at that location (if any)
     */
    public int addVertex(Vertex3D v, boolean tidy, boolean mergeDuplicates)
    {
        if (!tidy && !mergeDuplicates)
        {
            // fast version (ideal to load organized data)
            vertices.add(v);
            return vertices.size() - 1;
        }
        
        Integer index, nullIndex = -1;
        
        for (index = 0; index < vertices.size(); index++)
        {
            Vertex3D existingVertex = vertices.get(index);
            
            if (existingVertex == null)
            {
                // The current position in the vertex buffer is null.
                // To avoid growing the data array, this position
                // can be reused to store a new vertex (if needed)
                nullIndex = index;
            }
            else if (mergeDuplicates && existingVertex.position.epsilonEquals(v.position, 0.00001))
            {
                return index;
            }
        }
        
        // if there is a free spot in the list, use it
        if (tidy && nullIndex >= 0)
        {
            index = nullIndex;
            vertices.set(index, v);
        }
        else
        {
            vertices.add(v);
        }
        
        return index;
    }
    
    /**
     * Creates a new vertex for this mesh (without adding it), using the specified 3D coordinates.
     * <br/>
     * <br/>
     * This method is internally used when calling {@link #addVertex(Point3d)}, allowing overriding
     * classes to provide their own {@link Vertex3D} implementation.
     * 
     * @param position
     *            the position of the created vertex
     * @return the newly created vertex
     */
    public Vertex3D createVertex(Point3d position)
    {
        return new Vertex3D(position);
    }
    
    /**
     * @param index
     * @return the vertex at the specified index in the buffer
     */
    public Vertex3D getVertex(int index)
    {
        return vertices.get(index);
    }
    
    /**
     * @return the size of the vertex buffer
     * @param excludeNullElements
     *            set to <code>true</code> if <code>null</code> elements should not be counted
     *            (yielding the actual number of vertices, which may be smaller than the buffer
     *            size)
     */
    public int getNumberOfVertices(boolean excludeNullElements)
    {
        if (!excludeNullElements) return vertices.size();
        
        int count = 0;
        for (Vertex3D v : vertices)
            if (v != null) count++;
        return count;
    }
    
    /**
     * @return a shallow copy of the list of vertices. Adding or removing elements from this list
     *         will not affect the mesh, however modifying the vertices inside this list will.
     */
    public List<Vertex3D> getVertices()
    {
        List<Vertex3D> shallowCopy = new ArrayList<Vertex3D>(vertices.size());
        shallowCopy.addAll(vertices);
        return shallowCopy;
    }
    
    /**
     * Replaces the vertex at the specified index in the vertex buffer
     * 
     * @param index
     *            the index of the vertex to replace
     * @param vertex
     *            the new vertex
     */
    public void setVertex(int index, Vertex3D vertex)
    {
        vertices.set(index, vertex);
    }
    
    /**
     * Checks mesh integrity and will print out to the console any missing vertex
     * 
     * @return <code>true</code> if the check succeeded.
     */
    public boolean checkMeshIntegrity()
    {
        boolean success = true;
        
        for (C cell : cells)
            for (int i : cell.vertexIndices)
                if (vertices.get(i) == null)
                {
                    success = false;
                    System.err.println("missing vertex : " + i);
                }
            
        return success;
    }
    
    /**
     * Optimizes the vertex buffer by shifting all elements such that there are no <code>null</code>
     * elements intermingled between vertices.
     * 
     * @param tighten
     *            <ul>
     *            <li>if <code>true</code>: the vertex buffer is resized after optimization to
     *            ensure it does not contain empty values anymore</li>
     *            <li>if <code>false</code>: the size of the vertex buffer remains unchanged,
     *            therefore <code>null</code> elements may be found at the end of the buffer after
     *            optimization.</li>
     *            </ul>
     */
    public void optimizeVertexBuffer()
    {
        int nRealVerts = getNumberOfVertices(true);
        
        if (nRealVerts == vertices.size()) return;
        
        ArrayList<Vertex3D> newVertices = new ArrayList<Vertex3D>(nRealVerts);
        
        int blankSpaces = 0;
        
        int nVerts = vertices.size();
        
        for (int i = 0; i < nVerts; i++)
        {
            Vertex3D vertex = vertices.get(i);
            
            // count the number of consecutive blank spaces
            if (vertex == null)
            {
                blankSpaces++;
                continue;
            }
            
            newVertices.add(vertex);
            
            if (blankSpaces > 0)
            {
                // first available space = i - blankSpaces
                
                // redirect all cells to this space
                for (C cell : cells)
                    cell.replace(i, i - blankSpaces);
            }
        }
        
        setVertexData(newVertices);
        
        roiChanged(true);
    }
    
    /**
     * @return The major axis of this contour, i.e. an unnormalized vector linking the two most
     *         distant contour points (the orientation of this vector is arbitrary)
     */
    public Vector3d getMajorAxis()
    {
        Vector3d axis = new Vector3d();
        int nbPoints = vertices.size();
        
        // TODO this is not optimal, geometric moments should be used
        
        double maxDistSq = 0;
        Vector3d vec = new Vector3d();
        
        for (int i = 0; i < nbPoints; i++)
        {
            Vertex3D v1 = vertices.get(i);
            if (v1 == null) continue;
            
            for (int j = i + 1; j < nbPoints; j++)
            {
                Vertex3D v2 = vertices.get(j);
                if (v2 == null) continue;
                
                vec.sub(v1.position, v2.position);
                
                double dSq = vec.lengthSquared();
                
                if (dSq > maxDistSq)
                {
                    maxDistSq = dSq;
                    axis.set(vec);
                }
            }
        }
        
        return axis;
    }
    
    /**
     * @param pixelSize
     *            the size of a pixel in metric space. This information is necessary to perform
     *            conversions from metric to image space.
     */
    public void setPixelSize(Tuple3d pixelSize)
    {
        this.pixelSize.set(pixelSize);
    }
    
    /**
     * @return the pixel size associated to this mesh
     */
    public Tuple3d getPixelSize()
    {
        return new Point3d(pixelSize);
    }
    
    public Point3d getMassCenter(boolean convertToImageSpace)
    {
        return convertToImageSpace ? new Point3d(massCenter.x / pixelSize.x, massCenter.y / pixelSize.y, massCenter.z / pixelSize.z) : new Point3d(massCenter);
    }
    
    /**
     * Calculates Returns a 3D cuboid representing the contour's bounding box. The bounding box is
     * defined as the smallest cuboid that entirely contains the contour.
     * 
     * @return a {@link icy.type.rectangle.Rectangle3D.Double} object containing the bounding box of
     *         this mesh, expressed in voxel (image) space
     */
    public Rectangle3D computeBounds3D()
    {
        if (pixelSize.x == 0 || pixelSize.y == 0 || pixelSize.z == 0)
        {
            throw new RuntimeException("Invalid pixel size: (" + pixelSize.x + "," + pixelSize.y + "," + pixelSize.z + ')');
        }
        
        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE, minZ = Double.MAX_VALUE;
        double maxX = 0, maxY = 0, maxZ = 0;
        
        for (Vertex3D v : getVertices())
        {
            if (v == null) continue;
            
            if (v.position.x < minX) minX = v.position.x;
            if (v.position.x > maxX) maxX = v.position.x;
            if (v.position.y < minY) minY = v.position.y;
            if (v.position.y > maxY) maxY = v.position.y;
            if (v.position.z < minZ) minZ = v.position.z;
            if (v.position.z > maxZ) maxZ = v.position.z;
        }
        
        // switch from real to voxel space
        minX /= pixelSize.x;
        minY /= pixelSize.y;
        minZ /= pixelSize.z;
        maxX /= pixelSize.x;
        maxY /= pixelSize.y;
        maxZ /= pixelSize.z;
        
        return new Rectangle3D.Double(minX, minY, minZ, maxX - minX + 1, maxY - minY + 1, maxZ - minZ + 1);
    }
    
    /**
     * Rotates this mesh by a specified angle in radians. The axis of rotation (specified in the
     * parameter) is assumed to pass through the center of mass of this mesh, and the rotation angle
     * is assumed anti-clockwise when facing the vector.
     * 
     * @param axisAngle
     *            the axis and angle (in radians) of rotation
     */
    public void rotate(AxisAngle4d axisAngle)
    {
        Matrix3d r = new Matrix3d();
        r.set(axisAngle);
        
        Point3d center = getMassCenter(false);
        
        for (Vertex3D v : vertices)
        {
            if (v == null) continue;
            
            // translate to the center of rotation
            v.position.sub(center);
            // rotate
            r.transform(v.position);
            // translate back to place
            v.position.add(center);
        }
        
        roiChanged(true);
    }
    
    @Override
    public boolean canTranslate()
    {
        return true;
    }
    
    @Override
    public void translate(double dx, double dy, double dz)
    {
        translate(new Vector3d(dx, dy, dz));
    }
    
    /**
     * Translates (every vertex of) this mesh by the specified vector
     * 
     * @param vector
     *            the translation vector
     */
    public void translate(Vector3d vector)
    {
        if (vector.lengthSquared() == 0.0) return;
        
        for (Vertex3D v : vertices)
            if (v != null) v.position.add(vector);
        roiChanged(true);
    }
    
    @Override
    public boolean contains(double x, double y, double z, double sizeX, double sizeY, double sizeZ)
    {
        // the specified box is contained if all of its corners are
        
        if (!contains(x, y, z)) return false;
        if (!contains(x + sizeX, y, z)) return false;
        if (!contains(x, y + sizeY, z)) return false;
        if (!contains(x, y, z + sizeZ)) return false;
        if (!contains(x + sizeX, y + sizeY, z)) return false;
        if (!contains(x + sizeX, y, z + sizeZ)) return false;
        if (!contains(x, y + sizeY, z + sizeZ)) return false;
        if (!contains(x + sizeX, y + sizeY, z + sizeZ)) return false;
        
        return true;
    }
    
    @Override
    public boolean hasSelectedPoint()
    {
        return false;
    }
    
    @Override
    public boolean intersects(double x, double y, double z, double sizeX, double sizeY, double sizeZ)
    {
        // the specified box intersects if any of its corners do
        
        if (contains(x, y, z)) return true;
        if (contains(x + sizeX, y, z)) return true;
        if (contains(x, y + sizeY, z)) return true;
        if (contains(x, y, z + sizeZ)) return true;
        if (contains(x + sizeX, y + sizeY, z)) return true;
        if (contains(x + sizeX, y, z + sizeZ)) return true;
        if (contains(x, y + sizeY, z + sizeZ)) return true;
        if (contains(x + sizeX, y + sizeY, z + sizeZ)) return true;
        
        return false;
    }
}
