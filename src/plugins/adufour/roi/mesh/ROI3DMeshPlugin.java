package plugins.adufour.roi.mesh;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Marks the entire package as an Icy plugin
 * 
 * @author Alexandre Dufour
 */
public class ROI3DMeshPlugin extends Plugin implements PluginLibrary
{   
    
}
