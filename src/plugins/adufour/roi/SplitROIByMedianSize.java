package plugins.adufour.roi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import icy.math.ArrayMath;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.roi.ROI;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;

public class SplitROIByMedianSize extends Plugin implements ROIBlock, PluginBundled
{
    VarROIArray inputROI, outputROI;
    
    /**
     * Computes the median ROI size based on the given list, and splits the large ROI as many times
     * as this median size fits. The number of splits is rounded to the closest integer.<br/>
     * Example: if the median ROI size is 100px, ROI smaller than 150px will not be split, ROI
     * between 150px and 249px will be split into 2 pieces, etc.
     * 
     * @param rois
     *            the list of ROI to potentially split
     * @return an array containing a) the original, non-split ROI and b) the newly-split ROI
     */
    public static ROI[] splitByMedianSize(List<ROI> rois)
    {
        return splitByMedianSize(rois, true);
    }
    
    /**
     * Computes the median ROI size based on the given list, and splits the large ROI as many times
     * as this median size fits. The number of splits is rounded to the closest integer.<br/>
     * Example: if the median ROI size is 100px, ROI smaller than 150px will not be split, ROI
     * between 150px and 249px will be split into 2 pieces, etc.
     * 
     * @param rois
     *            the list of ROI to potentially split
     * @param recursive
     *            set to <code>true</code> if the median size estimation and splitting process
     *            should be repeated until the median stabilizes and no more split can be found
     * @return an array containing a) the original, non-split ROI and b) the newly-split ROI
     */
    public static ROI[] splitByMedianSize(List<ROI> rois, boolean recursive)
    {
        int nROI = rois.size();
        
        // The new ROI list is surely as big as the initial one
        List<ROI> splitROI = new ArrayList<ROI>(nROI);

        // Calculate the median size
        double[] volumes = new double[nROI];
        for (int i = 0; i < nROI; i++)
            volumes[i] = rois.get(i).getNumberOfPoints();
        double median = ArrayMath.median(volumes, true);
        
        for (ROI roi : rois)
        {
            double ratio = roi.getNumberOfPoints() / median;
            int nbObjects = (int) Math.round(ratio);
            if (nbObjects >= 2) // Split 
            {
                ROI[] splits = SplitROI.split(roi, nbObjects);
                splitROI.addAll(Arrays.asList(splits));
            }
            else // don't split, just add to the output list
            {
                splitROI.add(roi);
            }
        }
        
        int nNewROI = rois.size();
        
        // Recursion?
        if (recursive && nNewROI != nROI) return splitByMedianSize(splitROI, true);
        
        return splitROI.toArray(new ROI[nNewROI]);
    }
    
    /**
     * Computes the median ROI size based on the given list, and splits the large ROI as many times
     * as this median size fits<br/>
     * Example: if the median ROI size is 100, ROI of size under 200 will not be split, ROI of size
     * between 200 and 299 will be split into 2 pieces, etc.
     * 
     * @param rois
     *            the list of ROI to potentially split
     * @return an array containing the split ROI as well as the original ROI that did not need to be
     *         split
     */
    public static ROI[] splitByMedianSize(ROI[] rois)
    {
        return splitByMedianSize(Arrays.asList(rois));
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("inputROI", inputROI = new VarROIArray("List of ROI"));
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("outputROI", outputROI = new VarROIArray("List of split ROI"));
    }
    
    @Override
    public void run()
    {
        outputROI.setValue(splitByMedianSize(Arrays.asList(inputROI.getValue(true))));
    }
    
    @Override
    public String getMainPluginClassName()
    {
        return SplitROI.class.getName();
    }
}