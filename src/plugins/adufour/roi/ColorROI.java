package plugins.adufour.roi;

import icy.image.colormap.FireColorMap;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.JETColorMap;
import icy.roi.ROI;
import icy.sequence.Sequence;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarSequence;

public class ColorROI extends EzPlug
{
    EzVarSequence       seq = new EzVarSequence("sequence");
    EzVarEnum<ColorMap> map = new EzVarEnum<ColorMap>("Color map", ColorMap.values());
    
    public enum ColorMap
    {
        FIRE, JET
    }
    
    @Override
    public void execute()
    {
        Sequence s = seq.getValue(true);
        
        ArrayList<ROI> rois = s.getROIs();
        if (rois.size() == 0) return;
        
        IcyColorMap colorMap = null;
        
        switch (map.getValue())
        {
        case FIRE:
            colorMap = new FireColorMap();
            break;
        case JET:
            colorMap = new JETColorMap();
            break;
        }
        
        ROI[] _rois = rois.toArray(new ROI[rois.size()]);
        Arrays.sort(_rois, new Comparator<ROI>()
        {
            @Override
            public int compare(ROI o1, ROI o2)
            {
                return o1.getNumberOfContourPoints() < o2.getNumberOfContourPoints() ? -1 : 1;
            }
        });
        
        for (int i = 0; i < _rois.length; i++)
        {
            int index = (int) (i * IcyColorMap.SIZE / (double) _rois.length);
            _rois[i].setColor(new Color(colorMap.getRed(index), colorMap.getGreen(index), colorMap.getBlue(index)));
        }
    }
    
    @Override
    public void clean()
    {
    }
    
    @Override
    protected void initialize()
    {
        addEzComponent(seq);
        addEzComponent(map);
    }
}
